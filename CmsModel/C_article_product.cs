﻿/**  版本信息模板在安装目录下，可自行修改。
* C_article_product.cs
*
* 功 能： N/A
* 类 名： C_article_product
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2015/4/16 16:19:26   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace Cms.Model
{
	/// <summary>
	/// C_article_product:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class C_article_product
	{
		public C_article_product()
		{}
		#region Model
		private int _id;
		private int? _article_id;
		private decimal? _price;
		private decimal? _marketprice;
		private int? _integral;
		private int? _stock;
		private int? _is_integral;
		private int? _group_id=0;
		private int? _s_version;
		private string _style;
		private int? _type;
		private string _kim_joong;
		private string _inch_number;
		private decimal? _gold_price;
		private decimal? _labor_charges;
		private string _color;
		private string _clarity;
		private string _cut;
		private decimal? _main_stone_weight;
		private string _main_stone_number;
		private decimal? _total_weight;
		private decimal? _sales_labor_charge;
		private decimal? _accessories_price;
		private string _product_category;
		private decimal? _auxiliary_stone_weight;
		private string _side_stone_number;
		private string _certificate_no;
		private string _finger_ring;
		private int? _userscore;
		private string _diamond_shape;
		private string _material;
		private string _bstype;
		private string _pintype;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 文章ID
		/// </summary>
		public int? article_id
		{
			set{ _article_id=value;}
			get{return _article_id;}
		}
		/// <summary>
		/// 优惠价格
		/// </summary>
		public decimal? price
		{
			set{ _price=value;}
			get{return _price;}
		}
		/// <summary>
		/// 市场价格
		/// </summary>
		public decimal? marketPrice
		{
			set{ _marketprice=value;}
			get{return _marketprice;}
		}
		/// <summary>
		/// 积分
		/// </summary>
		public int? integral
		{
			set{ _integral=value;}
			get{return _integral;}
		}
		/// <summary>
		/// 库存
		/// </summary>
		public int? stock
		{
			set{ _stock=value;}
			get{return _stock;}
		}
		/// <summary>
		/// 是否开启积分兑换
		/// </summary>
		public int? is_integral
		{
			set{ _is_integral=value;}
			get{return _is_integral;}
		}
		/// <summary>
		/// 会员组ID
		/// </summary>
		public int? group_id
		{
			set{ _group_id=value;}
			get{return _group_id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? s_version
		{
			set{ _s_version=value;}
			get{return _s_version;}
		}
		/// <summary>
		/// 款号
		/// </summary>
		public string style
		{
			set{ _style=value;}
			get{return _style;}
		}
		/// <summary>
		/// 类型  黄金类  镶嵌类
		/// </summary>
		public int? type
		{
			set{ _type=value;}
			get{return _type;}
		}
		/// <summary>
		/// 金重
		/// </summary>
		public string kim_joong
		{
			set{ _kim_joong=value;}
			get{return _kim_joong;}
		}
		/// <summary>
		/// 寸数
		/// </summary>
		public string inch_number
		{
			set{ _inch_number=value;}
			get{return _inch_number;}
		}
		/// <summary>
		/// 金价
		/// </summary>
		public decimal? gold_price
		{
			set{ _gold_price=value;}
			get{return _gold_price;}
		}
		/// <summary>
		/// 工费
		/// </summary>
		public decimal? labor_charges
		{
			set{ _labor_charges=value;}
			get{return _labor_charges;}
		}
		/// <summary>
		/// 颜色
		/// </summary>
		public string color
		{
			set{ _color=value;}
			get{return _color;}
		}
		/// <summary>
		/// 净度
		/// </summary>
		public string clarity
		{
			set{ _clarity=value;}
			get{return _clarity;}
		}
		/// <summary>
		/// 切工
		/// </summary>
		public string cut
		{
			set{ _cut=value;}
			get{return _cut;}
		}
		/// <summary>
		/// 主石重
		/// </summary>
		public decimal? main_stone_weight
		{
			set{ _main_stone_weight=value;}
			get{return _main_stone_weight;}
		}
		/// <summary>
		/// 主石数量
		/// </summary>
		public string main_stone_number
		{
			set{ _main_stone_number=value;}
			get{return _main_stone_number;}
		}
		/// <summary>
		/// 总重
		/// </summary>
		public decimal? total_weight
		{
			set{ _total_weight=value;}
			get{return _total_weight;}
		}
		/// <summary>
		/// 销售工费
		/// </summary>
		public decimal? sales_labor_charge
		{
			set{ _sales_labor_charge=value;}
			get{return _sales_labor_charge;}
		}
		/// <summary>
		/// 配件价
		/// </summary>
		public decimal? accessories_price
		{
			set{ _accessories_price=value;}
			get{return _accessories_price;}
		}
		/// <summary>
		/// 产品种类
		/// </summary>
		public string product_category
		{
			set{ _product_category=value;}
			get{return _product_category;}
		}
		/// <summary>
		/// 辅石重
		/// </summary>
		public decimal? auxiliary_stone_weight
		{
			set{ _auxiliary_stone_weight=value;}
			get{return _auxiliary_stone_weight;}
		}
		/// <summary>
		/// 辅石数
		/// </summary>
		public string side_stone_number
		{
			set{ _side_stone_number=value;}
			get{return _side_stone_number;}
		}
		/// <summary>
		/// 证书号
		/// </summary>
		public string certificate_no
		{
			set{ _certificate_no=value;}
			get{return _certificate_no;}
		}
		/// <summary>
		/// 指圈
		/// </summary>
		public string finger_ring
		{
			set{ _finger_ring=value;}
			get{return _finger_ring;}
		}
		/// <summary>
		/// 会员所需积分
		/// </summary>
		public int? userscore
		{
			set{ _userscore=value;}
			get{return _userscore;}
		}
		/// <summary>
		/// 钻石形状
		/// </summary>
		public string diamond_shape
		{
			set{ _diamond_shape=value;}
			get{return _diamond_shape;}
		}
		/// <summary>
		/// 金属材质
		/// </summary>
		public string material
		{
			set{ _material=value;}
			get{return _material;}
		}
		/// <summary>
		/// 宝石类
		/// </summary>
		public string bstype
		{
			set{ _bstype=value;}
			get{return _bstype;}
		}
		/// <summary>
		/// 品类
		/// </summary>
		public string pintype
		{
			set{ _pintype=value;}
			get{return _pintype;}
		}
		#endregion Model

	}
}

