//index.js

//获取应用实例
var app = getApp()
var url = "https://xcxweb.ganzhouwei/api/weixin/index.aspx";
Page({
  data: {
    spzp_item: [],
    onReady: function (res) {
      this.videoContext = wx.createVideoContext('myVideo')
    },
    inputValue: '',
    data: {
      src: '',
    },
    /**
        * 页面配置
        */
    winWidth: 0,
    winHeight: 0,
    // tab切换
    currentTab: 0,
    hidden: true,
    scrollTop: 0,
    scrollHeight: 0,
    page: 1,
    isAjax: true,
    isGet: true
  },
  onLoad: function (options) {
    var that = this;

    /**
     * 获取系统信息
     */
    wx.getSystemInfo({

      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight,
          clientHeight: res.windowHeight,
          scrollHeight: res.windowHeight,
          parentId: options.parentId
        });
      }

    });
    this.loadMore();
  },
  //页面滑动到底部
  bindDownLoad: function () {
    if (this.data.isAjax && this.data.isGet) {
      this.loadMore();
    }
    console.log("123");
  },
  loadMore: function () {
    var that = this;
    this.setData({
      hidden: false,
      isGet: false
    });
    app.ajaxRequest({
      url: url,
      data: {
        opt: 'getArticle',
        parentId: that.data.parentId,
        page: that.data.page++
      },
      success: function (res) {
        console.info(res.data.ds);
        var list = that.data.spzp_item;
        if (res.data.status == 0) {
          for (var i = 0; i < res.data.ds.length; i++) {
            list.push(res.data.ds[i]);
          }
          that.setData({
            spzp_item: list,
            hidden: true,
            isGet: true
          });
          if (res.data.ds.length < 3) {
            that.data.isAjax = false;
          }
        }
        else {
          that.setData({
            hidden: true,
            isGet: false
          });
        }

      }
    })
  },
  
})