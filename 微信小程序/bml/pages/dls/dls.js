//index.js

//获取应用实例
var app = getApp()
Page({
  data: {
    dls_item: [],
    onReady: function (res) {
      this.videoContext = wx.createVideoContext('myVideo')
    },
    inputValue: '',
    data: {
      src: '',
    },
    /**
        * 页面配置
        */
    winWidth: 0,
    winHeight: 0,
    // tab切换
    currentTab: 0,
    hidden: true,
    scrollTop: 0,
    scrollHeight: 0,
    page: 1,
    isAjax: true,
    isGet: true
  },
  calling: function (e) {
    var that = this;
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.mobile, //此号码并非真实电话号码，仅用于测试
      success: function () {
        console.log("拨打电话成功！")
      },
      fail: function () {
        console.log("拨打电话失败！")
      }
    })
  },
  onLoad: function (options) {
    var that = this;
    
    /**
     * 获取系统信息
     */
    wx.getSystemInfo({

      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight,
          clientHeight: res.windowHeight,
          scrollHeight: res.windowHeight,
          parentId: options.parentId
        });
      }

    });
    this.loadMore();
  },
  //页面滑动到底部
  bindDownLoad: function () {
    if (this.data.isAjax && this.data.isGet) {
      this.loadMore();
    }
    console.log("123");
  },
  scroll: function (event) {
    //该方法绑定了页面滚动时的事件，我这里记录了当前的position.y的值,为了请求数据之后把页面定位到这里来。
    this.setData({
      scrollTop: event.detail.scrollTop
    });
  },
  topLoad: function (event) {
    //   该方法绑定了页面滑动到顶部的事件，然后做上拉刷新
    page = 0;
    this.setData({
      list: [],
      scrollTop: 0
    });
    if (this.data.isAjax && this.data.isGet) {
      this.loadMore();
    }
    console.log("lower");
  },
  loadMore: function () {
    var that = this;
    this.setData({
      hidden: false,
      isGet: false
    });
    app.ajaxRequest({
      url: app.url,
      data: {
        opt: 'getArticle',
        parentId: that.data.parentId,
        page: that.data.page++,
        pageSize:15
      },
      success: function (res) {
        console.info(res.data.ds);
        var list = that.data.dls_item;
        if (res.data.status == 0) {
          for (var i = 0; i < res.data.ds.length; i++) {
            list.push(res.data.ds[i]);
          }
          that.setData({
            dls_item: list,
            hidden: true,
            isGet: true
          });
          if (res.data.ds.length < 3) {
            that.data.isAjax = false;
          }
        }
        else {
          that.setData({
            hidden: true,
            isGet: false
          });
        }

      }
    })
  },
  /**
     * 滑动切换tab
     */
  bindChange: function (e) {

    var that = this;
    that.setData({ currentTab: e.detail.current });

  },
  /**
   * 点击tab切换
   */
  swichNav: function (e) {

    var that = this;

    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  }
})