//index.js
//获取应用实例
var app = getApp()
Page( {
  data: {
    mztj_item:[
      { "id":"1",
        "pic_url": 'https://xcxweb.ganzhouweixin.cn/images/tj1.png',
        "title": "斑美拉第二代",
        "price": "￥1288-9800",
        "gwc_url":'https://xcxweb.ganzhouweixin.cn/images/gwc.png'
      },{
        "id":"2",
        "pic_url": 'https://xcxweb.ganzhouweixin.cn/images/tj2.png',
        "title": "斑美拉第三代",
        "price": "￥158-899",
        "gwc_url":'https://xcxweb.ganzhouweixin.cn/images/gwc.png'
      },{
        "id":"3",
        "pic_url": 'https://xcxweb.ganzhouweixin.cn/images/tj3.png',
        "title": "内调产品，小分子低聚肽",
        "price": "￥1580-5660",
        "gwc_url":'https://xcxweb.ganzhouweixin.cn/images/gwc.png'
      }
    ],
    /**
        * 页面配置
        */
    winWidth: 0,
    winHeight: 0,
    // tab切换
    currentTab: 0,
  },
  onLoad: function() {
    var that = this;

    /**
     * 获取系统信息
     */
    wx.getSystemInfo( {

      success: function( res ) {
        that.setData( {
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        });
      }

    });
  },
  /**
     * 滑动切换tab
     */
  bindChange: function( e ) {

    var that = this;
    that.setData( { currentTab: e.detail.current });

  },
  /**
   * 点击tab切换
   */
  swichNav: function( e ) {

    var that = this;

    if( this.data.currentTab === e.target.dataset.current ) {
      return false;
    } else {
      that.setData( {
        currentTab: e.target.dataset.current
      })
    }
  }
})