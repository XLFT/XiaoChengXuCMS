//app.js
App({
  url: "https://xcxweb.ganzhouweixin.cn/api/weixin/index.aspx",
  webUrl: "https://xcxweb.ganzhouweixin.cn",
  onLaunch: function () {
    //调用API从本地缓存中获取数据
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
  },
  getUserInfo:function(cb){
    var that = this
    if(this.globalData.userInfo){
      typeof cb == "function" && cb(this.globalData.userInfo)
    }else{
      //调用登录接口
      wx.login({
        success: function () {
          wx.getUserInfo({
            success: function (res) {
              that.globalData.userInfo = res.userInfo
              typeof cb == "function" && cb(that.globalData.userInfo)
            }
          })
        }
      })
    }
  },
  globalData:{
    userInfo:null
  },
  ajaxRequest: function(options){
      wx.request({
        url: options.url,
        data: options.data,
        success: function(res){
          options.success(res);
        }
      })
  },
})