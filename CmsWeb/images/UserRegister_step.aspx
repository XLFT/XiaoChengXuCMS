﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserRegister_step.aspx.cs" Inherits="UserRegister" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <title></title>
    <script type="text/jscript">
        function ck() {
            var pwd = document.getElementById("tbpwd").value;
            var pwd2 = document.getElementById("tbpwd2").value;
            var tname = document.getElementById("tbname").value;
            var bl=true;
            if (pwd.length == 0) {
                alert("请输入登录密码！");
                return false;
            }
            if (pwd.length < 5) {
                alert("登录密码不能小于六位！");
                return false;
            }
            if (pwd != pwd2) {
                alert("确认密码与登录密码不一致！");
                return false;
            }
            if (tname.length == 0 || tname == '会员名') {
                alert("请输入会员名！");
                return false;
            }
//            return bl;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table>
    <tr><th>开通新会员</th></tr>
    <tr>
    <td>
      登录名称<asp:Literal ID="lbname" runat="server"></asp:Literal></td>
    </tr>
    <tr>
    <td>登录密码<asp:TextBox ID="tbpwd" runat="server" TextMode="Password"></asp:TextBox></td>
    </tr>
     <tr>   
    <td>确认密码<asp:TextBox ID="tbpwd2" runat="server" TextMode="Password"></asp:TextBox></td>
    </tr>
    <tr>   
    <td>会&nbsp;员&nbsp;名<asp:TextBox ID="tbname" runat="server"  onfocus="if (value =='会员名'){value =''}" onblur="if  (value ==''){value='会员名'}"></asp:TextBox></td>
    </tr>
     <tr>   
    <td align="center"><asp:Button ID="btsave" runat="server" Text="立即注册" onclick="btsave_Click" OnClientClick="return ck()"  /></td>
    </tr>
     <tr>   
    <td align="center">有会员卡？立即绑定！</td>
    </tr>
    </table>
    </div>
    </form>
</body>
</html>
