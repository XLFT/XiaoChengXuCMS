﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Cms.Common;
using System.Text;
using System.Threading;
public partial class _Default : System.Web.UI.Page
{
    System.Timers.Timer Timers_Timer = new System.Timers.Timer();
    public static int t = 0;
    static double DEF_PI = 3.14159265359; // PI
    static double DEF_2PI= 6.28318530712; // 2*PI
    static double DEF_PI180= 0.01745329252; // PI/180.0
    static double DEF_R =6370693.5; // radius of earth
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Timers_Timer.Interval = 1000;
            Timers_Timer.Enabled = true;
            Timers_Timer.Elapsed += new System.Timers.ElapsedEventHandler(bind);
          
        }
        double mLat1 = 23.134521; // point1纬度
        double mLon1 = 113.358803; // point1经度
        double mLat2 = 23.135186;// point2纬度
        double mLon2 = 113.358677;// point2经度
        double distance = GetShortDistance(mLon1, mLat1, mLon2, mLat2);
        Response.Write(distance);
        double lat = 28.197500, lng = 113.010150;
        double lat2 = 28.196740, lng2 = 113.010330;
        //double distance = GetShortDistance(lng, lat, lng2, lat2);
        //  getwxmap(out lat, out lng);
        // GetDistance(lat,lng,lat2,lng2);
        Response.Write(distance);
        wxuser.UserSale wu = new wxuser.UserSale();
        wxuser.userinfo xw = new wxuser.userinfo();
        wu = wxuser.getUserRegister("oLrWUtwciIdM2RXG5JHdjiFd-en4", "测试账号5", "M", "长沙", "2000-01-01", "2015-01-15", "15211119482","");
        Response.Write(wu.content + "-" + wu.result);


        xw = wxuser.getUserUpdate("oLrWUtwciIdM2RXG5JHdjiFd-en4", "75152111194821", "测试", "M", "长沙", "2010-01-05", "2015-01-10", "13511102022","");

        Response.Write(xw.content + "-" + xw.result + "||");

        xw = wxuser.getuserinfo("oLrWUtwciIdM2RXG5JHdjiFd-en4");
        Response.Write(xw.content + "-" + xw.result + "-" + xw.openid + "-" + xw.sex + "-" + xw.shopname + "-" + xw.telephone + "-" + xw.useraddress + "-" + xw.userallscore + "-" + xw.usercard + "-" + xw.userlevel + "-" + xw.username + "-" + xw.userscore + "-" + xw.allbuy + "-" + xw.birthday + "-" + xw.buytimes);

        xw = wxuser.getUserSign("oLrWUtwciIdM2RXG5JHdjiFd-en4", "75152111194821");
        Response.Write(xw.content + "-" + xw.result + "||");
        List<wxuser.UserSale> list = new List<wxuser.UserSale>();
        list = wxuser.getUserSale("oLrWUtwciIdM2RXG5JHdjiFd-en4", "75152111194821");
        for (int i = 0; i < list.Count; i++)
        {
            wxuser.UserSale ux = new wxuser.UserSale();
            ux = list[i];
            Response.Write(ux.content + "-" + ux.result);
        }
        xw = wxuser.getUserBind("oLrWUtwciIdM2RXG5JHdjiFd-en4", "15211119482", "75152111194821");
        Response.Write(xw.content + "-" + xw.result + "||");
        Response.Write("**" + "http://124.172.120.181:8090/wechat/ChatServlet?UserUpdate={\"openid\":\"oLrWUtwciIdM2RXG5JHdjiFd-en9\",\"usercard\":\"751\",\"username\":\"" + TransferEncoding(Encoding.Default, Encoding.UTF8, "测试") + "\",\"sex\":\"M\",\"useraddress\":\"" + TransferEncoding(Encoding.Default, Encoding.UTF8, "长沙") + "\",\"birthday\":\"2010-01-25\",\"marryday\":\"2015-01-09\",\"telephone\":\"15233354561\"}");
        Response.Write(Asc("q"));
        Response.Write(Chr(97));
        Response.Write(Guid.Parse("926b0219-fd2f-4515-829e-3303d61ee296") + "--");
        DateTime id = DateTime.Now; Response.Write(id.ToOADate()); 
        TextBox1.Text = t.ToString();
    }

    public void bind(object sender, System.Timers.ElapsedEventArgs e)
    {
        t++;
        TextBox1.Text = t.ToString();
       // DateTime id = DateTime.Now; Response.Write(id.ToOADate()); 
        if (t == 10)
        {
           // wxuser.setstatistics("","244","333",1,1);
        }
    }

    //字符转数字
    public static int Asc(string character)
    {
        if (character.Length == 1)
        {
            System.Text.ASCIIEncoding asciiEncoding = new System.Text.ASCIIEncoding();
            int intAsciiCode = (int)asciiEncoding.GetBytes(character)[0];
            return (intAsciiCode);
        }
        else
        {
            throw new Exception("Character is not valid.");
        }

    }

    //ASCII码转字符：

    public static string Chr(int asciiCode)
    {
        if (asciiCode >= 0 && asciiCode <= 255)
        {
            System.Text.ASCIIEncoding asciiEncoding = new System.Text.ASCIIEncoding();
            byte[] byteArray = new byte[] { (byte)asciiCode };
            string strCharacter = asciiEncoding.GetString(byteArray);
            return (strCharacter);
        }
        else
        {
            throw new Exception("ASCII Code is not valid.");
        }
    }

    public static string TransferEncoding(Encoding srcEncoding, Encoding dstEncoding, string srcStr)
    {
        byte[] srcBytes = srcEncoding.GetBytes(srcStr);
        byte[] bytes = Encoding.Convert(srcEncoding, dstEncoding, srcBytes);
        return dstEncoding.GetString(bytes);
    }
    public static void getwxmap(out double lat, out double lng)
    {
        lat = 0;
        lng = 0;
        var x_pi = 3.14159265358979324 * 3000.0 / 180.0;
        var x = lng;
        var y = lat;
        var z = Math.Sqrt(x * x + y * y) + 0.00002 * Math.Sin(y * x_pi);
        var theta = Math.Atan2(y, x) + 0.000003 * Math.Cos(x * x_pi);
        lng = z * Math.Cos(theta) + 0.0065;
        lat = z * Math.Sin(theta) + 0.006;
    }

    public static double GetDistance(double lat1, double lng1, double lat2, double lng2)
    {
        double EARTH_RADIUS = 6378.137;//地球的半径
        double radLat1 = rad(lat1);
        double radLat2 = rad(lat2);
        double a = radLat1 - radLat2;
        double b = rad(lng1) - rad(lng2);
        double s = 2 * Math.Asin(Math.Sqrt(Math.Pow(Math.Sin(a / 2), 2) +
         Math.Cos(radLat1) * Math.Cos(radLat2) * Math.Pow(Math.Sin(b / 2), 2)));
        s = s * EARTH_RADIUS;
        s = Math.Round(s * 10000) / 10000;
        s = s * 1000;
        return Math.Ceiling(s);
    }

//    public static double mToKm(int number){
//        string v="";
//    if(!Utils.IsNumeric(number)) return ' ';
//    switch (number){
//       case number>1800&&number<=2000:
//       v="2";
//       break;
//       case number>1500&&number<=1800:
//       v="1.8";
//       break;
//       case number>1200&&number<=1500:
//       v="1.5";
//       break;
//       case number>1000&&number<=1200:
//       v="1.2";
//       break;
//       case number>900&&number<=1000:
//       v="1";
//       break;
//       default:
//        v=Math.Ceiling(number/100)*100;
//       break; 
//    }
    
//    if($v<100){
//   $v= '距离我【<font color="#FF4C06"><b>'.$v.'</b></font>】千米内。';}
//   else{
//   $v= '距离我【<font color="#FF4C06"><b>'.$v.'</b></font>】米内。';
//    }
//    return $v;
    
//}

    public static double rad(double d)  
    {  
     return d * 3.1415926535898 / 180.0;  
    } 

    public double GetShortDistance(double lon1, double lat1, double lon2, double lat2)
    {
        double ew1, ns1, ew2, ns2;
        double dx, dy, dew;
        double distance;
        // 角度转换为弧度
        ew1 = lon1 * DEF_PI180;
        ns1 = lat1 * DEF_PI180;
        ew2 = lon2 * DEF_PI180;
        ns2 = lat2 * DEF_PI180;
        // 经度差
        dew = ew1 - ew2;
        // 若跨东经和西经180 度，进行调整
        if (dew > DEF_PI)
            dew = DEF_2PI - dew;
        else if (dew < -DEF_PI)
            dew = DEF_2PI + dew;
        dx = DEF_R * Math.Cos(ns1) * dew; // 东西方向长度(在纬度圈上的投影长度)
        dy = DEF_R * (ns1 - ns2); // 南北方向长度(在经度圈上的投影长度)
        // 勾股定理求斜边长
        distance = Math.Sqrt(dx * dx + dy * dy);
        return distance;
    }
    public double GetLongDistance(double lon1, double lat1, double lon2, double lat2)
    {
        double ew1, ns1, ew2, ns2;
        double distance;
        // 角度转换为弧度
        ew1 = lon1 * DEF_PI180;
        ns1 = lat1 * DEF_PI180;
        ew2 = lon2 * DEF_PI180;
        ns2 = lat2 * DEF_PI180;
        // 求大圆劣弧与球心所夹的角(弧度)
        distance = Math.Sin(ns1) * Math.Sin(ns2) + Math.Cos(ns1) * Math.Cos(ns2) * Math.Cos(ew1 - ew2);
        // 调整到[-1..1]范围内，避免溢出
        if (distance > 1.0)
            distance = 1.0;
        else if (distance < -1.0)
            distance = -1.0;
        // 求大圆劣弧长度
        distance = DEF_R * Math.Acos(distance);
        return distance;
    }

}