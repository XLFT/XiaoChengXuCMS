﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using Cms.Common;
public partial class wx_msg : System.Web.UI.Page
{
    public static string xtopenid = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.BLL.wx_info info = new Cms.BLL.wx_info();
            string appid = info.GetModel(1).AppId;
            string appsecret = info.GetModel(1).AppSecret;
            string url = Request.Url.Host.ToString();
            string redirectUrl = "http://" + url + "/wx/msg.aspx";
         
            if (Request.QueryString["code"] != null && Request.QueryString["code"] != "")
            {
              
                string Code = Request.QueryString["code"].ToString();
                string Str = GetJson("https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + appid + "&secret=" + appsecret + "&code=" + Code + "&grant_type=authorization_code");
                //微信回传的数据为Json格式，将Json格式转化成对象  
                OAuth_Token Model =wxuser.JSONToObject<OAuth_Token>(Str);
              
                xtopenid = Model._openid;
                openid.Value = Model._openid;
            }
            else
            {
                string posturl = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appid + "&redirect_uri=" + redirectUrl + "&response_type=code&scope=snsapi_base&state=123#wechat_redirect ";
                Response.Redirect(posturl);
            }

           
        }
    }

    /// <summary>
    /// 接受信息
    /// </summary>
    /// <param name="url">目标连接地址</param>
    /// <returns></returns>
    protected string GetJson(string url)
    {
        WebClient wc = new WebClient();
        wc.Credentials = CredentialCache.DefaultCredentials;
        wc.Encoding = Encoding.UTF8;
        string returnText = wc.DownloadString(url);
        if (returnText.Contains("errcode"))
        {
            //可能发生错误  
        }
        return returnText;
    }

    public class OAuth_Token
    {
        public OAuth_Token()
        {
            //  
            //TODO: 在此处添加构造函数逻辑  
            //  
        }
        //access_token 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同  
        //expires_in access_token接口调用凭证超时时间，单位（秒）  
        //refresh_token 用户刷新access_token  
        //openid 用户唯一标识，请注意，在未关注公众号时，用户访问公众号的网页，也会产生一个用户和公众号唯一的OpenID  
        //scope 用户授权的作用域，使用逗号（,）分隔  
        public string _access_token;
        public string _expires_in;
        public string _refresh_token;
        public string _openid;
        public string _scope;
        public string access_token
        {
            set { _access_token = value; }
            get { return _access_token; }
        }
        public string expires_in
        {
            set { _expires_in = value; }
            get { return _expires_in; }
        }
        public string refresh_token
        {
            set { _refresh_token = value; }
            get { return _refresh_token; }
        }
        public string openid
        {
            set { _openid = value; }
            get { return _openid; }
        }
        public string scope
        {
            set { _scope = value; }
            get { return _scope; }
        }
    }  
}