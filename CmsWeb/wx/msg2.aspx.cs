﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class wx_msg2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        { 
             string reurl = "http://yiwang007.gotoip1.com/wx/msg.aspx?id=2";
             string code = "";
            if (Request.QueryString["code"] != null && Request.QueryString["code"] != "")
            {
                //获取微信回传的code
                code = Request.QueryString["code"].ToString();  
               
                if (code!= "")  //已获取得openid及其他信息
                {
                    //在页面上输出用户信息
                    wxuser.setstatistics("", "接受信息2", Request.QueryString["code"].ToString(), 0, 1);
                    //或跳转到自己的页面，想怎么处理就怎么处理
                    Response.Redirect(reurl);
                }
                else  //未获得openid，回到wxProcess.aspx，访问弹出微信授权页面
                {
                    Response.Redirect("msg.aspx?auth=1");
                }
            }
            wxuser.setstatistics("", "接受信息3", "跳入页面", 0, 1);
        }
    }
}