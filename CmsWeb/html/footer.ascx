﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="footer.ascx.cs" Inherits="html_footer" %>
<div class="footer">
  <div class="indmain footpd">
    <div class="fl-lt">
      <div class="footnav"><a href="/" target="_blank">主页</a><a href="html/page.aspx?id=93" target="_blank">关于我们</a><a href="html/product.aspx?id=42" target="_blank">产品系列</a><a href="html/product.aspx?id=103" target="_blank">成功案例</a><a href="html/page.aspx?id=116" target="_blank">联系我们</a></div>
      <div class="footinfo">版权所有：<%=Application["Copyright"]%></div>
    </div>
    <ul class="footrt">
      <li>
        <div class="footicon footi-1"></div>
        <p><%=Application["email"]%></p>
      </li>
      <li>
        <div class="footicon footi-2"></div>
        <p><%=Application["telphone"]%></p>
      </li>
      <li>
        <div class="footicon footi-3"></div>
        <p><%=Application["adress"]%></p>
      </li>
    </ul>
  </div>
</div>