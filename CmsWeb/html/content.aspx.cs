﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Cms.DBUtility;

public partial class html_content : System.Web.UI.Page
{
    public DataSet ds;
    public SqlDataAdapter dr;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int id = Convert.ToInt32(this.Request.QueryString["articleId"] ?? "0");//ID
           
            if (new Cms.BLL.C_article().Exists(id))
            {
                Cms.Model.C_article model = new Cms.BLL.C_article().GetModel(id);
                if (model != null)
                {
                    Application["newTitle"] = model.title;
                    Application["newContent"] = model.content;

                    Application["newIntro"] = model.intro;
                    
                    if (new Cms.BLL.C_Column().Exists(Convert.ToInt32(model.parentId)))
                    {
                        Cms.Model.C_Column modelC_Column = new Cms.BLL.C_Column().GetModel(Convert.ToInt32(model.parentId));
                        if (modelC_Column != null)
                        {
                            Application["className"] = modelC_Column.className;
                            Application["content"] = modelC_Column.content;
                            Application["engName"] = modelC_Column.engName;
                            Application["intro"] = modelC_Column.intro;
                            Application["sub_title"] = modelC_Column.sub_title;
                        }


                    }
                }


            }
            else
            {
                Response.Redirect("/index.aspx");
            }
        }

        DataSet ds = new Cms.BLL.C_Column().GetList("parentId=26 order by classId");
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            RepeaterClumn.DataSource = ds.Tables[0].DefaultView;
            RepeaterClumn.DataBind();
        }
    }


    protected void RepeaterClumn_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rep = e.Item.FindControl("RepeaterClumnSub") as Repeater;//找到里层的repeater对象
            DataRowView rowv = (DataRowView)e.Item.DataItem;//找到分类Repeater关联的数据项 
            int classId = Convert.ToInt32(rowv["classId"]); //获取填充子类的id 
            rep.DataSource = new Cms.BLL.C_Column().GetList("parentId=" + classId + " order by classId").Tables[0].DefaultView;
            rep.DataBind();
        }
    }
}