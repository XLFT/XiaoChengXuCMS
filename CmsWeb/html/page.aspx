﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="page.aspx.cs" Inherits="html_page" %>
<%@ Register Src="~/html/footer.ascx" TagName="footer" TagPrefix="uc1" %>
<%@ Register Src="~/html/header.ascx" TagName="header" TagPrefix="uc2" %>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><%=Application["title"]%></title>
<meta name="Keywords" content="<%=Application["keyword"]%>" />
<meta name="Description" content="<%=Application["Description"]%>" />
<link type="text/css" href="css/style.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/jquery.tool.js"></script>
<script type="text/javascript" src="js/focus.js"></script>
</head>

<body>
<form runat="server">
<uc2:header ID="header1" runat="server" />
<!--内页banner-->
<div class="inside-banner"></div>
<div class="indwarp">
  <div class="indmain">
    <dl class="inside-nav">
      <asp:Repeater ID="RepeaterClumn" runat="server" 
            onitemdatabound="RepeaterClumn_ItemDataBound">
    <ItemTemplate>
      <dt><%#Eval("className")%></dt>
      <dd>
      <asp:Repeater ID="RepeaterClumnSub" runat="server">
    <ItemTemplate>
      <a href="<%#ToAspx.getCloumnUrl(Convert.ToInt32(Eval("classId")))%>" target="_blank"><%#Eval("className")%></a>
      </ItemTemplate>
   </asp:Repeater>
      </dd>
    </ItemTemplate>
   </asp:Repeater> 
    </dl>
    <div class="inside-rt">
      <div class="inlt-tit">
        <h3><a href="javascript:;" target="_blank"><%=Application["className"] %></a></h3>
         <p><%=Application["engName"]%></p>
      </div>
      <div class="prodpage">
        <h2><%=Application["sub_title"]%></h2>
        <div class="prod-p-info">
          <%=Application["content"]%>
        </div>
      </div>
    </div>
  </div>
</div>
<uc1:footer ID="footer1" runat="server" />
</form>
</body>
</html>
