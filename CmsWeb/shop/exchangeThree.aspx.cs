﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_exchangeThree : System.Web.UI.Page
{
    protected int user_id;//会员id

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();
            this.user_id = userModel.id;
            Application["usercard"] = userModel.usercard;
            Bind_date(user_id);//赋值操作
          
        }
    }
    #region 赋值操作========================
    public void Bind_date(int user_id)
    {
        DataTable dt = new Cms.BLL.C_order_integral().GetList("user_id=" + user_id + " order by id desc").Tables[0];
        if (dt != null && dt.Rows.Count > 0)
        {
            RepOrderList.DataSource = dt;
            RepOrderList.DataBind();
        }
        else
        {
            RepOrderList.DataSource = dt;
            RepOrderList.DataBind();
        }

        
    }

    public void RepOrderList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rep = e.Item.FindControl("RepOrderListSub") as Repeater;//找到里层的repeater对象
            DataRowView rowv = (DataRowView)e.Item.DataItem;//找到分类Repeater关联的数据项 
            int id = Convert.ToInt32(rowv["id"]); //获取填充子类的id 

            DataSet dsNavOne = new Cms.BLL.C_order_integralsub().GetList("order_id=" + id + " order by id desc");
            if (dsNavOne != null && dsNavOne.Tables[0].Rows.Count > 0)
            {
                rep.DataSource = dsNavOne.Tables[0].DefaultView;
                rep.DataBind();
            }
        }
    }
    
    /// <summary>
    /// 获取产品封面图
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getPhoto(string article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            result = new Cms.BLL.C_article().GetModel(Convert.ToInt32(article_id)).photoUrl.ToString();
        }
        return result;
    }

    public string getState(int order_state)
    {
        string result = "";
        switch (order_state)
        {
            case 0:
                result = "马上预约";
                break;
            case 1:
                result = "已预约";
                break;
            case 2:
                result = "已领取";
                break;
        }

        return result;
    }
    public string getStateCss(int order_state)
    {
        string result = "";
        switch (order_state)
        {
            case 0:
                result = "color1";
                break;
            case 1:
                result = "";
                break;
            case 2:
                result = "";
                break;
        }

        return result;
    }
    #endregion

    #region 马上预约=====================
    protected void lbedit_Command(object sender, CommandEventArgs e)
    {
        string id = e.CommandArgument.ToString();
        id = e.CommandName.ToString();
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        Cms.Model.C_order_integral model = new Cms.BLL.C_order_integral().GetModel(Convert.ToInt32(id));
        model.order_status = Convert.ToInt32("1");
        
           
            DataTable dt=new Cms.BLL.C_order_integralsub().GetList("order_id="+Convert.ToInt32(id)).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                try
                {
                    //wxuser.UserSale wu = new wxuser.UserSale();
                    //wu = wxuser.getUserScore(userModel.usercard, userModel.openid, model.order_num, "兑换产品", "-" + model.note);
                    wxuser.GiftBook gB = new wxuser.GiftBook();
                    gB = wxuser.getGiftBook(model.storesId.ToString(), userModel.openid, userModel.username, userModel.telphone, dt.Rows[0]["title"].ToString(), "1", model.quantity_sum.ToString(), model.integral_sum.ToString(), string.Format("{0:yyyy-MM-dd}", model.updateTime), string.Format("{0:yyyy-MM-dd}", model.updateTime), model.order_num.ToString());
                    
                    if (gB.result == "更新成功")
                    {
                        if (new Cms.BLL.C_order_integral().Update(model))
                        {
                            JscriptMsg("预约成功！", "/shop/exchangeThree.aspx", "Success");
                        }
                        else
                        {
                            JscriptMsg("预约失败！", "/shop/exchangeThree.aspx", "Error");
                        }
                    }
                    else
                    {
                        JscriptMsg("预约失败！", "/shop/exchangeThree.aspx", "Error");
                    }
            
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                    Response.End();
                }
            }
            else
            {
                JscriptMsg("预约失败！", "/shop/exchangeThree.aspx", "Error");
            }
               
            
       
    }
    #endregion

    #region 提示框=================================
    public void JscriptMsg(string msgtitle, string url, string msgcss)
    {
        string msbox = "";
        if (url == "back")
        {
            msbox = "jsdialog(\"提示\", \"" + msgtitle + "\",\"" + url + "\", \"\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }
        else
        {
            msbox = "jsprintWeb(\"" + msgtitle + "\", \"" + url + "\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }

    }
    #endregion
}