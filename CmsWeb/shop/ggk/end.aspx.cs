﻿using Cms.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class shop_ggk_end : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       // OnlyWeiXinLook();

        if (!IsPostBack)
        {
            int id = MyCommFun.RequestInt("aid");
            if (id == 0)
            {
                return;
            }
            Cms.BLL.wx_ggkActionInfo aBll = new Cms.BLL.wx_ggkActionInfo();
            Cms.Model.wx_ggkActionInfo action = aBll.GetModel(id);
            if (action == null)
            {
                return;
            }
            litEndNotice.Text = action.endContent;
        }
    }
}