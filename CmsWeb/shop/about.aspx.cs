﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class shop_about : System.Web.UI.Page
{
    public int classId = 117;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            classId = Convert.ToInt32(this.Request.QueryString["id"] ?? "117");//文章ID
            Cms.BLL.C_Column bllcolumn = new Cms.BLL.C_Column();
            Cms.Model.C_Column model = bllcolumn.GetModel(classId);
            if (model != null)
            {
                Application["ArticleTitle"] = model.className;
                Application["ArticleContent"] = model.content;
            }
        }
    }
}