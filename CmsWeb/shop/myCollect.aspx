﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="myCollect.aspx.cs" Inherits="shop_myCollect" %>
<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.1.11.1.js"></script>
    <script type="text/javascript">
        (function ($) {
            $.fn.extend({
                "jTab": function (o) {
                    o = $.extend({
                        menutag: "", 	//选项卡按钮标签
                        boxtag: "",     //选项卡内容标签
                        cur: 0, 	  	//选项卡的默认索引值
                        act: "mouseover", //事情触发的默认事件可以是"click"
                        fade: 0, 		//淡入的时间
                        auto: false, //false,true表示开关定时器
                        autoTime: 3000	//定时执行的时间
                    }, o)
                    $(o.menutag).eq(0).addClass("cur");
                    $(o.boxtag).eq(0).siblings().hide();
                    $(o.menutag).bind(o.act, function () {
                        index = $(o.menutag).index(this);
                        $(this).addClass("cur").siblings().removeClass("cur")
                        $(o.boxtag).eq(index).show(o.fade).siblings().hide();
                    })

                }
            })
        })(jQuery);
    </script>
    <script type="text/javascript">
        $(function () {
            $(".tab").jTab({
                menutag: ".tab-m>li",
                boxtag: ".tab-box>div",
                cur: 0,
                act: "mouseover",
                fade: 0,
                auto: false,
                autoTime: 3000
            })
        })
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0);" class="page-title">我的收藏</a>
    </header>
    <section class="main main-product">
        <div class="p-contain">
        	<div class="tab">
                <ul class="tab-m">
                  <li><a href="javascript:void(0)"><span>产品</span></a></li>
                  <li><a href="javascript:void(0)"><span>积分</span></a></li>
                  <li><a href="javascript:void(0)"><span>裸钻</span></a></li>
                  <div class="clear"></div>
                </ul>
                <div class="tab-box">
                    <div class="thfnxw">
                        <div class="mine-order">
                            <ul>
                            <asp:Repeater runat="server" ID="RepOrderList">
                            <ItemTemplate>
                                 <li>
                                   <h2><span>卡号：<%=Application["usercard"]%></span>收藏时间：<%#Eval("updateTime")%></h2>
                                    <div class="m-order-goods">
                                        <div class="m-order-goods1">
                                            <a href="<%#getUrl(Eval("id").ToString())%>">
                                                <img src="<%#getPhoto(Eval("article_id").ToString())%>" class="m-oeder-img">
                                                <div class="m-order-name">
                                                    <h3><%#Eval("title")%></h3>
                                                    <p><span> <%#Eval("integral").ToString() == "0" ? "" : "<strong>积分兑换：</strong>" + Eval("integral") + "积分"%>
                                    <%#Convert.ToDecimal(Eval("price")).ToString("0.00") == "0.00" ? "" : " <strong> 价格：¥ </strong>" + Convert.ToDecimal(Eval("price")).ToString("0.00")%> </span></p>
                                                   
                                                </div>
                                                <div class="clear"></div>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                            <%#RepOrderList.Items.Count == 0 ? "<div style='text-align:center;'>暂无收藏产品记录</div>" : ""%>
                            </FooterTemplate>
                            </asp:Repeater> 
                            </ul>
                        </div>
                    </div>
                    <div class="thfnxw">
                        <div class="mine-order">
                            <ul>
                           <asp:Repeater runat="server" ID="RepIsNo">
                            <ItemTemplate>
                                <li>
                                   <h2><span>卡号：<%=Application["usercard"]%></span>收藏时间：<%#Eval("updateTime")%></h2>
                                    <div class="m-order-goods">
                                        <div class="m-order-goods1">
                                            <a href="<%#getUrl(Eval("id").ToString())%>">
                                                <img src="<%#getPhoto(Eval("article_id").ToString())%>" class="m-oeder-img">
                                                <div class="m-order-name">
                                                    <h3><%#Eval("title")%></h3>
                                                    <p><span> <%#Eval("integral").ToString() == "0" ? "" : "<b>积分兑换：</b>" + Eval("integral") + "积分"%>
                                    <%#Convert.ToDecimal(Eval("price")).ToString("0.00") == "0.00" ? "" : " <b> 价格：¥ </b>" + Convert.ToDecimal(Eval("price")).ToString("0.00")%> </span></p>
                                                   
                                                </div>
                                                <div class="clear"></div>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                            <%#RepIsNo.Items.Count == 0 ? "<div style='text-align:center;'>暂无收藏积分记录</div>" : ""%>
                            </FooterTemplate>
                            </asp:Repeater> 
                            </ul>
                        </div>
                    </div>
                    <div class="thfnxw">
                        <div class="mine-order">
                            <ul>
                            <asp:Repeater runat="server" ID="RepIsYes">
                            <ItemTemplate>
                                 <li>
                                   <h2><span>卡号：<%=Application["usercard"]%></span>收藏时间：<%#Eval("updateTime")%></h2>
                                    <div class="m-order-goods">
                                        <div class="m-order-goods1">
                                            <a href="<%#getUrl(Eval("id").ToString())%>">
                                                <img src="<%#getPhoto(Eval("article_id").ToString())%>" class="m-oeder-img">
                                                <div class="m-order-name">
                                                    <h3><%#Eval("title")%></h3>
                                                    <p><span> <%#Eval("integral").ToString() == "0" ? "" : "<b>积分兑换：</b>" + Eval("integral") + "积分"%>
                                    <%#Convert.ToDecimal(Eval("price")).ToString("0.00") == "0.00" ? "" : " <b>价格：¥ </b>" + Convert.ToDecimal(Eval("price")).ToString("0.00")%> </span></p>
                                                </div>
                                                <div class="clear"></div>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                            <%#RepIsYes.Items.Count == 0 ? "<div style='text-align:center;'>暂无收藏裸钻记录</div>" : ""%>
                            </FooterTemplate>
                            </asp:Repeater> 
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
</section>
    </form>
</body>
</html>