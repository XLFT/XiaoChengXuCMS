﻿<%@ WebHandler Language="C#" Class="shop" %>

using System;
using System.IO;
using System.Net;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;

public class shop : IHttpHandler,IRequiresSessionState
{
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        string action = context.Request.QueryString["action"].ToString();
        switch (action)
        {
            case "sendSms"://发送短信
                sendSms(context);
                break;
            case "getTelphone": //验证手机账号是否存在
                getTelphone(context);
                break;  
             
        }
    }

    #region 发送短信==========================
    public class SmsApi
    {

        public string rescode { get; set; }

        public string message { get; set; }

    }
    public void sendSms(HttpContext context)
    {
        string result = "";
        string code=Cms.Common.Utils.Number(6);
        context.Session["Verification_code"] = code; //随机生成6位数字
        string telphone = context.Request.QueryString["telphone"].ToString();
        string content = "您当前的手机验证码：" + code;

        Cms.BLL.C_sms bll = new Cms.BLL.C_sms();
        Cms.Model.C_sms model = new Cms.Model.C_sms();
        model.name = "";//名字
        model.telphone = telphone;//手机号码
        model.content = content;//内容
        model.state = 0;//发送状态
        model.updateTime = Convert.ToDateTime(Cms.Common.ManagementInfo.GetTime());//时间
        int count = bll.Add(model);
        if (count > 0)
        {
            string smsResult = adminUser.Sms(telphone, content);
            SmsApi p = Cms.Common.Utils.JsonDeserialize<SmsApi>(smsResult);
            if (p.rescode == "0")
            {
            int counts = Cms.DBUtility.DbHelperSQL.ExecuteSql("update C_sms set state=1 where id=" + count);//修改状态
                result = code;
            }
            else
            {
                result = "0";
            }
           
        }
        else
        {
            result="0";
           
        }
        context.Response.Write(result);
        return;
    }
    #endregion

    #region 验证手机是否重复============================
    public void getTelphone(HttpContext context)
    {
       
        string telPhone = context.Request.QueryString["telphone"];
        ;
        if (!System.Text.RegularExpressions.Regex.IsMatch(telPhone, @"^((13[0-9])|(15[0-9])|(18[0-9])|(17[0-9])|(14[0-9])|188|189)[0-9]{8}$"))
        {
            context.Response.Write("3");//手机格式不对
            return;
        }
        if (string.IsNullOrEmpty(telPhone))
        {
            context.Response.Write("2");//手机不能为空
            return;
        }
        Cms.BLL.C_user cuser = new Cms.BLL.C_user();
        if (cuser.Exists(telPhone))
        {
            context.Response.Write("1");//已经注册了
            return;
        }
        context.Response.Write("0");//表示未注册
        return;
    }
    #endregion

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}