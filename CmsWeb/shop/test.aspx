﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="test.aspx.cs" Inherits="shop_test" %>

<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/admin/scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <script type="text/javascript" src="/admin/js/layout.js"></script>
</head>
<body>
    <form id="form1" runat="server">
  <%--  <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>--%>
            <section class="all">
    <header>
        <a href="javascript:void(0)" onclick="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0);" class="page-title">开通新会员</a>
    </header>

    <section class="main main-product">
        <div class="p-contain">
        	<div class="p-vip">
            	<p>为了你的账户安全请填写一个手机号码验证</p>
                <div class="vip-input">
                	<input name="" id="telPhone" runat="server" type="text" placeholder="请输入手机号码" onblur="getPhone();"/>
                </div>
                <div class="vip-input">
                	<input name="" id="VerificationCode" runat="server" type="text" placeholder="输入验证码"/>
                    <%--<asp:LinkButton ID="LinkButton1" runat="server" CssClass="vip-hq" 
                        onclick="LinkButton1_Click">获取验证码</asp:LinkButton>--%>
                        <input type="button" id="btnSendCode" value="获取验证码" class="vip-hq" onclick="sendMessage()" /> 
                      <%--  <a href="javascript:void(0)" onclick="SendMsg();" class="vip-hq">获取验证码</a>--%>
                </div>
                <div class="new-vipjj">
                    <asp:DropDownList ID="parentId" runat="server" datatype="*" sucmsg=" ">
                    </asp:DropDownList>
                </div>
                <div class="new-vip">
                	<input name="" type="checkbox" id="agree" runat="server" value=""/><label for="agree">同意<a href="javascript:void(0);">《DBE会员章程》</a></label>
                </div>
                <div class="product-button1">
                    <input id="HiddenField1" type="hidden" value="" runat="server" />
                     <input id="HiddenField2" type="hidden" value="" runat="server" />
                <asp:Button ID="Button1" runat="server"  CssClass="login_ipt" onclick="Button1_Click" ValidationGroup="submit" CausesValidation="True"  Text="确认开通"></asp:Button>
                </div>
            </div>
        </div>
     <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
    </section>
       <%-- </ContentTemplate>
    </asp:UpdatePanel>--%>
    </form>
</body>
<script type="text/javascript">
    function getPhone() {
        var telPhone = $("#telPhone").val();
        $.ajax({
            type: "POST",
            url: "ashx/shop.ashx?action=getTelphone&telphone=" + telPhone,
            data: "",
            success: function (msg) {
                if (msg == "0") {
                    jsprintWeb("该手机可使用！", "NotJump", "Success");
                }
                else if (msg == "1") {
                    jsprintWeb("该手机已被注册，请更换！", "NotJump", "Error");
                }
                else if (msg == "3") {
                    jsprintWeb("请输入有效手机号码！", "NotJump", "Error");
                }
                else {
                    jsprintWeb("手机不能为空，请填写！", "NotJump", "Error");
                }
            }
        });
    }
</script>
<script type="text/javascript">
    var InterValObj; //timer变量，控制时间
    var count = 60; //间隔函数，1秒执行
    var curCount; //当前剩余秒数
    function time() {
        curCount = count;
        $("#btnSendCode").attr("disabled", "true");
        $("#btnSendCode").val(curCount + "秒后可以重发");
        InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次
    }
    function sendMessage() {
        var telPhone = $("#telPhone").val();
        //向后台发送处理数据
        if (telPhone != "") {
            $.ajax({
                type: "POST",
                url: "ashx/shop.ashx?action=getTelphone&telphone=" + telPhone,
                data: "",
                success: function (msg) {
                    if (msg == "0") {
                        curCount = count;
                        //设置button效果，开始计时
                        $("#btnSendCode").attr("disabled", "true");
                        $("#btnSendCode").val(curCount + "秒后可以重发");
                        InterValObj = window.setInterval(SetRemainTime, 1000); //启动计时器，1秒执行一次
                        $.ajax({
                            type: "POST",
                            url: "ashx/shop.ashx?action=sendSms&telphone=" + telPhone,
                            data: "",
                            success: function (msg) {
                                if (msg == "0") {
                                    jsprintWeb("验证码发送失败！", "NotJump", "Error");
                                }
                                else {
                                    jsprintWeb("验证码发送成功！", "NotJump", "Success");
                                }

                            }
                        });
                    }
                    else if (msg == "1") {
                        jsprintWeb("该手机已被注册，请更换！", "NotJump", "Error");
                    }
                    else if (msg == "3") {
                        jsprintWeb("请输入有效手机号码！", "NotJump", "Error");
                    }
                    else {
                        jsprintWeb("手机不能为空，请填写！", "NotJump", "Error");
                    }
                }
            });


        }
        else {
            jsprintWeb("手机不能为空！", "NotJump", "Error");
        }
    }
    //timer处理函数
    function SetRemainTime() {
        if (curCount == 0) {
            window.clearInterval(InterValObj); //停止计时器
            $("#btnSendCode").removeAttr("disabled"); //启用按钮
            $("#btnSendCode").val("重新发送");
        }
        else {
            curCount--;
            $("#btnSendCode").val(curCount + "秒后可以重发");
        }
    }
</script>
</html>
