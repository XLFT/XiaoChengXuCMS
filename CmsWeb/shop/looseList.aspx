﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="looseList.aspx.cs" Inherits="shop_looseList" %>
<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
         <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0)" class="page-title">裸钻列表</a>
    </header>
    <section class="main main-product">
        <div class="p-contain">
        	<div class="results">
            	为您搜索到的2093颗钻石
            </div>
            <div class="search-list">
                <div class="filter">
                    <ul>
                        <li class="current"><a href="#">所有产品</a></li>
                        <li><a href="#">价格</a></li>
                        <li><a href="#">钻重</a></li>
                        <li><a href="#">销量</a></li>
                        <div class="clear"></div>
                    </ul>
                </div>
                <ul class="search-ul" id="gallery-filtralbe">
                <asp:Repeater ID="RepList" runat="server">
                <ItemTemplate>
                	<li>
                    	<a href="/shop/looseDetail.aspx?id=<%#Eval("articleId") %>">
                        	<img src="<%#Eval("photoUrl") %>">
                            <div class="search-li-r">
                                <div class="property">
                                    <p>钻重: <span><%#getLoose(Convert.ToInt32(Eval("articleId"))) %></span></p>
                                    <p>颜色: <span><%#getColor(Convert.ToInt32(Eval("articleId"))) %></span></p>
                                    <p>净度: <span><%#getClarity(Convert.ToInt32(Eval("articleId")))%></span></p>
                                    <p>切工: <span><%#getCut(Convert.ToInt32(Eval("articleId"))) %></span></p>
                                    <div class="clear"></div>
                                </div>
                                <div class="price">
                                    ￥<%#getPrice(Convert.ToInt32(Eval("articleId"))) %>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </a>
                    </li>
                </ItemTemplate>
                </asp:Repeater>
                </ul>
                <div class="product-button1 product-button2 product-button3">
                    <a href="tel:073184632168" class="login_ipt">微信客服</a>
                    <a href="tel:073184632168" class="login_ipt">电话客服</a>
                </div>
            </div>
             <input type="hidden" value="<%=classIdList%>" id="oncemore" />
             <a href="javascript:void(0);" class="more-p jia" id="dateTips"></a>
        </div>
     <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
</section>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            var isajax = true;
            var pageNow = 2;
            var classid = document.getElementById("oncemore").value;
            jQuery(window).scroll(function () {
                if (($(window).scrollTop()) >= ($(document).height() - $(window).height())) {
                    if (isajax) { jQuery(".jia").click(); }
                }
            });
            jQuery(".jia").click(function () {
                if (isajax) {
                    jQuery.ajax(
                    { type: "POST",
                        url: "looseMore.aspx?t=" + Math.random(),
                        data: { "page": pageNow, "classid": classid },
                        dataType: "html",
                        cache: true,
                        beforeSend: function () {
                            jQuery(".jia").addClass("loadboxing");
                            isajax = false;
                        },
                        success: function (data) {
                            if (data != '') {
                                jQuery("#gallery-filtralbe").append(data);
                                isajax = true;
                                pageNow++;
                            }
                            else {
                                isajax = false;
                                $("#dateTips").html("");
                                $("#dateTips").html("暂无更多相关数据");
                            }
                        },
                        error: function () { alert("暂无更多相关数据!"); }
                    })
                }
            });
        });
    </script>
    </form>
</body>
</html>
