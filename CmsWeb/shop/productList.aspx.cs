﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_productList : System.Web.UI.Page
{
    protected int user_id;//会员id
    protected string classIdList;
    protected string orderPrice;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Cms.Model.C_user userModel = adminUser.GetuserLoginState();//验证登录
            //this.user_id = userModel.id;
            string str = "";
            //产品列表
            if (Request["classid"] == null || Request["classid"].ToString() == "")
            {
                classIdList = ToAspx.getcloumnid(42) + "," + ToAspx.getcloumnid(56) + "," + ToAspx.getcloumnid(55) + "," + ToAspx.getcloumnid(54);
               
                classIdList = "parentId in (" + classIdList + ")";
                Application["type_id"] = "current";
                Application["type_yellow"] = "";
                Application["type_losse"] = "";
                Application["type_price"] = "";
                orderPrice = "0";
                str = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (order by T.orderNumber desc )AS Row, T.*  from View_product T WHERE " + classIdList + "  ) TT WHERE TT.Row between 0 and 4 ";
            }
            else
            {
                classIdList = ToAspx.getcloumnid(Convert.ToInt32(Request["classid"].ToString()));
                Application["parentId"] = Request["classid"].ToString();
                classIdList = "parentId in (" + classIdList + ")";
                Application["type_id"] = "current";
                Application["type_yellow"] = "";
                Application["type_losse"] = "";
                Application["type_price"] = "";
                orderPrice = "0";
                str = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (order by T.orderNumber desc )AS Row, T.*  from View_product T WHERE " + classIdList + "  ) TT WHERE TT.Row between 0 and 4 ";
            }
            if (Request["type_yellow"] != null)
            {
                Application["type_yellow"] = "current";
                Application["type_losse"] = "";
                Application["type_price"] = "";
                Application["type_id"] = "";
                orderPrice = "0";
                classIdList = classIdList + "and type=101";
                str = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (order by T.orderNumber desc )AS Row, T.*  from View_product T WHERE " + classIdList + "  ) TT WHERE TT.Row between 0 and 4 ";
            }
            if (Request["type_losse"] != null)
            {
                Application["type_yellow"] = "";
                Application["type_losse"] = "current";
                Application["type_price"] = "";
                Application["type_id"] = "";
                orderPrice = "0";
                classIdList = classIdList + " and type=102";
                str = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (order by T.orderNumber desc )AS Row, T.*  from View_product T WHERE " + classIdList + "  ) TT WHERE TT.Row between 0 and 4 ";
            }
            if (Request["type_price"] != null)
            {
                Application["type_yellow"] = "";
                Application["type_losse"] = "";
                Application["type_price"] = "current";
                Application["type_id"] = "";
                orderPrice = "1";
                str = "SELECT * FROM ( SELECT ROW_NUMBER() OVER (order by T.orderNumber desc )AS Row, T.*  from View_product T WHERE " + classIdList + "  ) TT WHERE TT.Row between 0 and 4  order by price asc";
            }
            
            BindDate(str);
        }
    }
    #region 赋值操作========================
    public void BindDate(string str)
    {
        DataSet dsProduct = Cms.DBUtility.DbHelperSQL.Query(str);
        if (dsProduct != null && dsProduct.Tables[0].Rows.Count > 0)
        {
            RepList.DataSource = dsProduct.Tables[0].DefaultView;
            RepList.DataBind();
        }
        else
        {
            RepList.DataSource = dsProduct.Tables[0].DefaultView;
            RepList.DataBind();
        }

    }
    protected void RepList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int id = Convert.ToInt32(((HiddenField)e.Item.FindControl("Fielddocid")).Value);
        switch (e.CommandName)
        {
            case "lbtnLess":
                update(id, "-");
                break;
        }
    }
    public void update(int id, string action)
    {

        Cms.BLL.C_collect bll = new Cms.BLL.C_collect();
        Cms.Model.C_collect model = new Cms.Model.C_collect();
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        this.user_id = userModel.id;

        int articleId = id;//文章ID
        DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + articleId).Tables[0];
        if (dt.Rows.Count > 0)
        {
            model.article_id = articleId;
            model.user_id = user_id;
            model.typeId = Convert.ToInt32("1");//收藏分类id
            model.title = new Cms.BLL.C_article().GetModel(articleId).title.ToString();
            model.price = Convert.ToDecimal(dt.Rows[0]["price"]);
            model.integral = Convert.ToInt32(dt.Rows[0]["userscore"].ToString() == "" ? "0" : dt.Rows[0]["userscore"].ToString());
            model.updateTime = DateTime.Now;
            if (Cms.DBUtility.DbHelperSQL.Exists("select count(1) from C_collect where article_id=" + articleId + "and user_id=" + user_id))
            {
                JscriptMsg("您已收藏过了！", "NotJump", "Error");
            }
            else
            {
                int result = bll.Add(model);
                if (result > 0)
                {
                    JscriptMsg("收藏成功！", "NotJump", "Success");
                }
                else
                {
                    JscriptMsg("收藏失败！", "NotJump", "Error");
                }
            }

        }
    }

    public string getPrice(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = Convert.ToDecimal(dt.Rows[0]["price"]).ToString("0.00");
            }
        }
        return result;
    }

    public string getMprice(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = Convert.ToDecimal(dt.Rows[0]["marketPrice"]).ToString("0.00");
            }
        }
        return result;
    }
    #endregion

    #region 提示框=================================
    public void JscriptMsg(string msgtitle, string url, string msgcss)
    {
        string msbox = "";
        if (url == "back")
        {
            msbox = "jsdialog(\"提示\", \"" + msgtitle + "\",\"" + url + "\", \"\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }
        else
        {
            msbox = "jsprintWeb(\"" + msgtitle + "\", \"" + url + "\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }

    }
    #endregion
}