﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_userEdit : System.Web.UI.Page
{
    protected int user_id;//会员id
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();
            this.user_id = userModel.id;
            
            DropList_Bind();
            Bind_date();

        }
    }
    #region 绑定门店=================================
    public void DropList_Bind()
    {
        //parentId.Items.Clear();
        List<wxuser.ShopInfo> list = new List<wxuser.ShopInfo>();

        list = wxuser.getShopInfo("ok");
        if (list != null && list.Count > 0)
        {
            for (int i = 0; i < list.Count; i++)
            {

                ListItem item = new ListItem();
                item.Text = list[i].shopname.ToString();
                item.Value = list[i].shopcode.ToString();
                parentId.Items.Add(item);
            }

        }

    }

    #endregion

    #region 赋值操作========================
    public void Bind_date()
    {
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        this.user_id = userModel.id;
        this.name.Value= userModel.username;
        Application["usercard"] = userModel.usercard;
        Application["userlevel"] = userModel.userlevel;
        Application["sex"] = userModel.sex;
        sex.SelectedValue = userModel.sex;
        birthday.Text = string.Format("{0:yyyy-MM-dd}", userModel.birthday);
        marryday.Text = string.Format("{0:yyyy-MM-dd}", userModel.marryday);
        Application["openid"] = userModel.openid;
        Application["telphone"] = userModel.telphone;
        useraddress.Value = userModel.useraddress;
        parentId.SelectedValue = userModel.shopcode.ToString();
    }
    #endregion

    #region 保存信息==============================
    protected void Button1_Click(object sender, EventArgs e)
    {
        Cms.BLL.C_user bll = new Cms.BLL.C_user();
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        this.user_id = userModel.id;
        userModel.id = user_id;
        userModel.username = name.Value;
        userModel.sex = sex.SelectedValue;
        userModel.birthday = Convert.ToDateTime(birthday.Text);
        userModel.marryday = Convert.ToDateTime(marryday.Text);
        userModel.useraddress = useraddress.Value;
        userModel.shopcode = parentId.SelectedValue.ToString();
        userModel.shopname = parentId.SelectedItem.Text.ToString();
        
        //int result = Cms.DBUtility.DbHelperSQL.ExecuteSql("update C_user set sex='" + userModel.sex + "', username='" + userModel.username + "', birthday='" + userModel.birthday + "',marryday='" + userModel.marryday + "',useraddress='" + userModel.useraddress + "' where id=" + userModel.id);
        if (bll.Update(userModel))
        {
            //数据同步CRM
            wxuser.userinfo xw = new wxuser.userinfo();
            xw = wxuser.getUserUpdate(userModel.openid, userModel.usercard, userModel.username, userModel.sex, userModel.useraddress, string.Format("{0:yyyy-MM-dd}", userModel.birthday), string.Format("{0:yyyy-MM-dd}", userModel.marryday), userModel.telphone, userModel.shopcode);
            if (xw.result == "更新成功")
            {
                JscriptMsg("保存成功！", "/shop/user.aspx", "Success");
            }
            else
            {
                JscriptMsg("保存失败！", "NotJump", "Error");
            }
        }
        else
        {
            JscriptMsg("保存失败！", "NotJump", "Error");
        }
    }
    #endregion
    #region 提示框=================================
    public void JscriptMsg(string msgtitle, string url, string msgcss)
    {
        string msbox = "";
        if (url == "back")
        {
            msbox = "jsdialog(\"提示\", \"" + msgtitle + "\",\"" + url + "\", \"\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }
        else
        {
            msbox = "jsprintWeb(\"" + msgtitle + "\", \"" + url + "\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }

    }
    #endregion
}