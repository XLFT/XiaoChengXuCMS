﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="exchangeOne.aspx.cs" Inherits="shop_exchangeOne" %>
<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="/admin/scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <script type="text/javascript" src="/admin/js/layout.js"></script>
    <script type="text/javascript">
        /**
        * 以下为html5代码,获取地理位置
        */
//        getLocation();
        function getLocation() {
            //检查浏览器是否支持地理位置获取
            if (navigator.geolocation) {
                //若支持地理位置获取,成功调用showPosition(),失败调用showError
                // alert("正在努力获取位置...");
                var config = { enableHighAccuracy: true, timeout: 5000, maximumAge: 30000 };
                navigator.geolocation.getCurrentPosition(showPosition, showError, config);
            } else {
                //alert("Geolocation is not supported by this browser.");
                alert("定位失败,用户已禁用位置获取权限");
            }
        }
        /**
        * 获取地址位置成功
        */
        function showPosition(position) {
            //获得经度纬度
            var x = position.coords.latitude;
            var y = position.coords.longitude;
            //配置Baidu Geocoding API
            var url = "http://api.map.baidu.com/geocoder/v2/?ak=mRGwVhDu56ZgKTONMSFkkc6m" + "&callback=renderReverse" + "&location=" + x + "," + y + "&output=json" + "&pois=0";
            $.ajax({
                type: "GET",
                dataType: "json",
                url: url,
                success: function (json) {
                    if (json == null || typeof (json) == "undefined") {
                        return;
                    }
                    if (json.status != "0") {
                        return;
                    }
                    document.getElementById("HiddenX").value = x;
                    document.getElementById("HiddenY").value = y;
                    document.getElementById("txtPosition").value = "[x:" + x + ",y:" + y + "]地址位置获取失败,请手动选择地址";
                    //setAddress(json.result.addressComponent);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    document.getElementById("HiddenX").value = x;
                    document.getElementById("HiddenY").value = y;
//                    alert("[x:" + x + ",y:" + y + "]地址位置获取失败,请手动选择地址");
                }
            });
        }
        /**
        * 获取地址位置失败[暂不处理]
        */
        function showError(error) {
            switch (error.code) {
                case error.PERMISSION_DENIED:
                    alert("定位失败,用户拒绝请求地理定位");
                    //x.innerHTML = "User denied the request for Geolocation.[用户拒绝请求地理定位]"
                    break;
                case error.POSITION_UNAVAILABLE:
                    alert("定位失败,位置信息是不可用");
                    //x.innerHTML = "Location information is unavailable.[位置信息是不可用]"
                    break;
                case error.TIMEOUT:
                    alert("定位失败,请求获取用户位置超时");
                    //x.innerHTML = "The request to get user location timed out.[请求获取用户位置超时]"
                    break;
                case error.UNKNOWN_ERROR:
                    alert("定位失败,定位系统失效");
                    //x.innerHTML = "An unknown error occurred.[未知错误]"
                    break;
            }
        }
        /**
        * 设置地址
        */
        function setAddress(json) {
            var position = document.getElementById("txtPosition");
            //省
            var province = json.province;
            //市
            var city = json.city;
            //区
            var district = json.district;
            province = province.replace('市', '');
            position.value = province + "," + city + "," + district;
            position.style.color = 'black';
        } 
	</script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0);" class="page-title">积分兑换</a>
    </header>
    <section class="main main-product">
        <div class="p-contain">
            <div class="mine-order gift-list">
                <ul>
                <asp:Repeater runat="server" ID="RepExchange" 
                        onitemcommand="RepExchange_ItemCommand">
                <ItemTemplate>
                    <li>
                        <h2>礼品清单<span>卡号：<%=Application["usercard"]%></span></h2>
                        <div class="m-order-goods">
                            <a href="/shop/integralDetail.aspx?id=<%#Eval("article_id") %>">
                                <img src="<%#getPhoto(Eval("article_id").ToString())%>" class="m-oeder-img">
                                <div class="m-order-name">
                                    <h3><%#Eval("title") %></h3>
                                    <p><span><b>积分兑换：</b><%#getIntegral(Convert.ToInt32(Eval("article_id")))%>积分<del> 价格：¥ <%#getMprice(Convert.ToInt32(Eval("article_id")))%> </del><i>数量：<%#Eval("quantity")%></i></span></p>
                                </div>
                                <div class="clear"></div>
                            </a>
                        &nbsp;</div>
                    </li>
                </ItemTemplate>
                </asp:Repeater>
                </ul>
            </div>
            <div class="bill-total">
                <p>共<%=QTotal%>件，<b>需<%=dintegral%>积分</b></p>
                <span>（剩余积分<%=Application["userscore"]%>）</span>
            <div style="display:none;">
            <input type="hidden" value="" id="txtPosition" name="txtPosition" />
            <asp:TextBox ID="HiddenX" runat="server" Text="154"></asp:TextBox>
            <asp:TextBox ID="HiddenY" runat="server" Text="56"></asp:TextBox>
            
             </div>
            
            </div>
            <div class="product-button1 product-button2">
<asp:LinkButton ID="LinkButton1" CssClass="login_ipt" runat="server" 
                    onclick="LinkButton1_Click">下一步</asp:LinkButton>
<asp:LinkButton ID="LinkButton2" CssClass="login_ipt-xg" runat="server" 
                    onclick="LinkButton2_Click">取&nbsp;&nbsp;&nbsp;消</asp:LinkButton>
            </div>
        </div>
    </section>
         <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
</section>
    </form>
</body>
</html>
