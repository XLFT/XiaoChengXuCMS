﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="orderDetail.aspx.cs" Inherits="shop_orderDetail" %>

<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.1.11.1.js"></script>
    <script type="text/javascript">
        (function ($) {
            $.fn.extend({
                "jTab": function (o) {
                    o = $.extend({
                        menutag: "", 	//选项卡按钮标签
                        boxtag: "",     //选项卡内容标签
                        cur: 0, 	  	//选项卡的默认索引值
                        act: "mouseover", //事情触发的默认事件可以是"click"
                        fade: 0, 		//淡入的时间
                        auto: false, //false,true表示开关定时器
                        autoTime: 3000	//定时执行的时间
                    }, o)
                    $(o.menutag).eq(0).addClass("cur");
                    $(o.boxtag).eq(0).siblings().hide();
                    $(o.menutag).bind(o.act, function () {
                        index = $(o.menutag).index(this);
                        $(this).addClass("cur").siblings().removeClass("cur")
                        $(o.boxtag).eq(index).show(o.fade).siblings().hide();
                    })

                }
            })
        })(jQuery);
    </script>
    <script type="text/javascript">
        $(function () {
            $(".tab").jTab({
                menutag: ".tab-m>li",
                boxtag: ".tab-box>div",
                cur: 0,
                act: "mouseover",
                fade: 0,
                auto: false,
                autoTime: 3000
            })
        })
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0);" class="page-title">订单详情</a>
    </header>
    <section class="main main-product">
        <div class="p-contain">
        	<div class="tab">
                <div class="tab-box">
                    <div class="thfnxw">
                        <div class="mine-order">
                        <div class="address">
                <div class="addre_c">
                    <div class="addre_cc">
                    <asp:Repeater runat="server" ID="RepAddress">
                    <ItemTemplate>
                    	<a href="/shop/EditAddress.aspx?action=edit&id=<%#Eval("id")%>&ReturnPage=/shop/Clearing.aspx">
                            <h3>收货人：<%#Eval("consignee")%><span><%#Eval("cellphone")%></span></h3>
                            <p>收货地址：<%#Eval("address")%></p>
                        </a>
                    </ItemTemplate>
                    </asp:Repeater>
                    </div>
                </div>
            </div>
                            <ul>
                                <li>
                                    <h2><p>下单时间：<%=Application["updateTime"]%>   <b style=" float:right;">数量：<%=Application["quantity_sum"]%></b></p>
                                    <p>订单编号：<%=Application["order_num"]%>   <b style=" float:right;">¥：<%=Application["price_sum"]%></b></p>
                                    </h2>
                            <asp:Repeater runat="server" ID="RepOrderListSub">
                            <ItemTemplate>
                                    <div class="m-order-goods">
                                        <div class="m-order-goods1">
                                            <a href="/shop/productDetail.aspx?id=<%#Eval("article_id")%>">
                                                <img src="<%#getPhoto(Eval("article_id").ToString())%>" class="m-oeder-img" alt="">
                                                <div class="m-order-name">
                                                    <h3><%#Eval("title")%></h3>
                                                    <p>价格：¥ <%#Convert.ToDecimal(Eval("price")).ToString("0.00")%><span>数量：<%#Eval("quantity")%></span></p>
                                                </div>
                                                <div class="clear"></div>
                                            </a>
                                        </div>
                                    </div>
                            </ItemTemplate>
                            </asp:Repeater> 
                            <div class="status">
                            <span><%=Application["is_payment"].ToString() == "0" ? "<a href='/api/wxpay/JsApiPayPage.aspx?orderid=" + Application["id"] + "' class='fukuan'>付款</a>" : "<a href='javascript:void(0);'>已支付</a>"%></span>
                            <span>  <%=getIsRefund(Application["is_refund"].ToString(), Application["id"].ToString())%></span>
                            <span><%=getIsDelivery(Application["is_delivery"].ToString(), Application["id"].ToString())%></span>
                            <span><%=Application["is_transaction"].ToString() == "0" ? "<a href='javascript:void(0);'>未完成</a>" : "<a href='javascript:void(0);'>已完成</a>"%></span>
                            </div>
                                </li>
                          
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
         <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
</section>
    </form>
</body>
</html>
