﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_main : System.Web.UI.Page
{
    protected int user_id;//会员id
    public Double user_money = 0.00;//会员账户余额
    public int userJifen;//会员账户积分
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();
            this.user_id = userModel.id;
            Cms.BLL.C_user bll = new Cms.BLL.C_user();
            Cms.Model.C_user model = new Cms.BLL.C_user().GetModel(user_id);
            wxuser.userinfo xw = new wxuser.userinfo();
            try
            {
                xw = wxuser.getuserinfo(model.openid);
                model.userallscore = Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userallscore)).ToString());
                model.userscore = Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userscore)).ToString());
                model.userYesScore = Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userallscore)).ToString()) - Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userscore)).ToString());
                model.userlevel = xw.userlevel;
                model.shopname = xw.shopname;
                bll.Update(model);
            }
            catch (Exception ex)
            {

            }
            if (model.usercard == "" || model.usercard == null)
            {
                bindUser.HRef = "/shop/bindUser.aspx";
                bindUser.InnerHtml = "绑定实体店会员卡！";
            }
            else
            {
                bindUser.HRef = "javascript:void(0)";
                bindUser.InnerHtml = "已绑定实体店会员卡！";
            }
            Application["username"] = model.username;
            Application["headimgurl"] = model.headimgurl;
            userJifen = Convert.ToInt32(new Cms.BLL.C_user().GetModel(user_id).userscore);//获取会员账户积分
            user_money = Convert.ToDouble(new Cms.BLL.C_user().GetModel(user_id).userMoney);//获取会员账户余额

            bind_order_count(user_id);
        }
    }

    public void bind_order_count(int user_id)
    {
        Application["payCount"] = Convert.ToInt32(Cms.DBUtility.DbHelperSQL.GetSingle("select count(*) from C_order where is_payment=0 and user_id=" + user_id));
        Application["is_delivery"] = Convert.ToInt32(Cms.DBUtility.DbHelperSQL.GetSingle("select count(*) from C_order where is_delivery=0 and user_id=" + user_id));
        Application["is_receiving"] = Convert.ToInt32(Cms.DBUtility.DbHelperSQL.GetSingle("select count(*) from C_order where is_receiving=1 and user_id=" + user_id));
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        adminUser.LoginOut();
    }
}