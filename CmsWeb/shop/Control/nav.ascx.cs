﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_Control_nav : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDate();
        }
    }
    #region 赋值操作========================
    public void BindDate()
    {
        DataSet dsNav = new Cms.BLL.C_Column().GetList("parentId=93 order by orderNumber desc");
        if (dsNav != null && dsNav.Tables[0].Rows.Count > 0)
        {
            RepNav.DataSource = dsNav.Tables[0].DefaultView;
            RepNav.DataBind();
        }
       

    }
    protected void RepNav_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rep = e.Item.FindControl("RepNavOne") as Repeater;//找到里层的repeater对象
            DataRowView rowv = (DataRowView)e.Item.DataItem;//找到分类Repeater关联的数据项 
            int classid = Convert.ToInt32(rowv["classid"]); //获取填充子类的id 

            DataSet dsNavOne = new Cms.BLL.C_Column().GetList("parentId=" + classid + " order by orderNumber desc");
            if (dsNavOne != null && dsNavOne.Tables[0].Rows.Count > 0)
            {
                rep.DataSource = dsNavOne.Tables[0].DefaultView;
                rep.DataBind();
            }
        }
    }
    #endregion
}