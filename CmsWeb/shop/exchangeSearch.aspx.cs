﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Net;

public partial class shop_exchangeSearch : System.Web.UI.Page
{
    protected int user_id;//会员id

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           

            int order_id = Convert.ToInt32(Request["id"].ToString());
            string userId = this.Request.QueryString["userId"] ?? "";//会员id
            string storesId = this.Request.QueryString["storesId"] ?? "";//门店id
            Cms.BLL.C_order_integral bll = new Cms.BLL.C_order_integral();
            Cms.Model.C_order_integral model = new Cms.BLL.C_order_integral().GetModel(order_id);
            model.order_status = Convert.ToInt32("2");
            bll.Update(model);
            Application["order_num"] = new Cms.BLL.C_order_integral().GetModel(order_id).order_num;
            Application["orderStatus"] =getState(Convert.ToInt32(new Cms.BLL.C_order_integral().GetModel(order_id).order_status));
            if (model.storesId.ToString() != "")
            {
                Cms.Model.sc_stores modelStores = new Cms.BLL.sc_stores().GetModel(Convert.ToInt32(model.storesId));
                storename.Text = modelStores.storename;
                address.Text = modelStores.address;
                tel.Text = modelStores.tel;
            }
            else
            {
                Cms.Model.sc_stores modelStores = new Cms.BLL.sc_stores().GetModel(Convert.ToInt32("1"));
                storename.Text = modelStores.storename;
                address.Text = modelStores.address;
                tel.Text = modelStores.tel;
            }
            Bind_date(order_id);//赋值操作
            
        }
    }
    #region 赋值操作========================
    public void Bind_date(int order_id)
    {
        DataTable dt = new Cms.BLL.C_order_integral().GetList("id=" + order_id).Tables[0];
        if (dt != null && dt.Rows.Count > 0)
        {
            RepOrderList.DataSource = dt;
            RepOrderList.DataBind();
        }
        else
        {
            RepOrderList.DataSource = dt;
            RepOrderList.DataBind();
        }


    }
    public string getState(int order_state)
    {
        string result = "";
        switch (order_state)
        {
            case 0:
                result = "马上预约";
                break;
            case 1:
                result = "已预约";
                break;
            case 2:
                result = "已提货";
                break;
        }

        return result;
    }
    public void RepOrderList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rep = e.Item.FindControl("RepOrderListSub") as Repeater;//找到里层的repeater对象
            DataRowView rowv = (DataRowView)e.Item.DataItem;//找到分类Repeater关联的数据项 
            int id = Convert.ToInt32(rowv["id"]); //获取填充子类的id 

            DataSet dsNavOne = new Cms.BLL.C_order_integralsub().GetList("order_id=" + id + " order by id desc");
            if (dsNavOne != null && dsNavOne.Tables[0].Rows.Count > 0)
            {
                rep.DataSource = dsNavOne.Tables[0].DefaultView;
                rep.DataBind();
            }
        }
    }

    /// <summary>
    /// 获取产品封面图
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getPhoto(string article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            result = new Cms.BLL.C_article().GetModel(Convert.ToInt32(article_id)).photoUrl.ToString();
        }
        return result;
    }

    #endregion

    
}