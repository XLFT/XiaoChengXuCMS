﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="exchangeThree.aspx.cs" Inherits="shop_exchangeThree" %>

<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="/admin/scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <script type="text/javascript" src="/admin/js/layout.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0);" class="page-title">积分兑换</a>
    </header>
    <section class="main main-product">
        <div class="p-contain">
        	<%--<div class="serch">
            	<input name="" type="text" placeholder="输入兑换单号可查询">
            </div>--%>
            <div class="mine-order gift-list">
                <ul>
                <asp:Repeater runat="server" ID="RepOrderList" 
                        OnItemDataBound="RepOrderList_ItemDataBound">
                <ItemTemplate>
                    <li>
                        <h2>兑换时间：<%#Eval("updateTime")%></h2>
                         <asp:Repeater runat="server" ID="RepOrderListSub">
                            <ItemTemplate>
                        <div class="m-order-goods">
                            <a href="/shop/integralDetail.aspx?id=<%#Eval("article_id")%>">
                                <img src="<%#getPhoto(Eval("article_id").ToString())%>" class="m-oeder-img" alt=""/>
                                <div class="m-order-name">
                                    <h3><%#Eval("title")%></h3>
                                    <p><span><b>积分兑换：</b><%#Eval("integral")%>积分<del> 价格：¥ <%#Convert.ToDecimal(Eval("price")).ToString("0.00")%> </del><i>数量：<%#Eval("quantity")%></i></span></p>
                                </div>
                                <div class="clear"></div>
                            </a>
                           
                        &nbsp;</div>
                            </ItemTemplate>
                            </asp:Repeater> 
                        <div class="m-order-edit">
                        <asp:LinkButton ID="lbedit" runat="server" CssClass='<%#getStateCss(Convert.ToInt32(Eval("order_status")))%>' CommandArgument='<%#Eval("id")%>' CommandName='<%#Eval("id")%>' oncommand="lbedit_Command"><%#getState(Convert.ToInt32(Eval("order_status")))%></asp:LinkButton>
                       <%#Eval("order_status").ToString()=="0" ? "":"<a href='/shop/exchangeFour.aspx?orderId="+Eval("id")+"' id='TiHuo' runat='server' class=''>提货卷</a>"%> 
                        </div>
                    </li>
                </ItemTemplate>
                </asp:Repeater>
                </ul>
            </div>
        </div>
    </section>
     <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
    </form>
</body>
</html>
