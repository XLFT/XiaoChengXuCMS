﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_exchangePass : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            

        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(this.Request.QueryString["id"] ?? "0");//订单ID
        string userId = this.Request.QueryString["userId"] ?? "";//会员id
        string storesId = this.Request.QueryString["storesId"] ?? "";//门店id
        //DataTable dt = new Cms.BLL.C_order_integral().GetList("id=" + id + " and user_id=" + userId + "and storesId=" + storesId).Tables[0];
        string Pass=txtPass.Value.Trim().ToString();
        Cms.Model.C_order_integral model = new Cms.BLL.C_order_integral().GetModel(id);
        DataTable dt = new Cms.BLL.sc_stores().GetList("id=" + model.storesId + " and verificationPass='" + Pass + "'").Tables[0];
        if (dt != null && dt.Rows.Count > 0)
        {
            Response.Redirect("/shop/exchangeSearch.aspx?id=" + id + "&user_id=" + userId + "and storesId=" + storesId);
        }
        else
        {
            JscriptMsg("核销密码错误！", "NotJump", "Error");
        }


    }
    #region 提示框=================================
    public void JscriptMsg(string msgtitle, string url, string msgcss)
    {
        string msbox = "";
        if (url == "back")
        {
            msbox = "jsdialog(\"提示\", \"" + msgtitle + "\",\"" + url + "\", \"\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }
        else
        {
            msbox = "jsprintWeb(\"" + msgtitle + "\", \"" + url + "\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }

    }
    #endregion
}