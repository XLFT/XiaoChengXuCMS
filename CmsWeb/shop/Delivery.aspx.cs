﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class shop_Delivery : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bind_date();
        }
    }
    public void bind_date()
    {
        int otid = Convert.ToInt32(Request["orderid"].ToString());
        Cms.BLL.C_order otBll = new Cms.BLL.C_order();
        Cms.Model.C_order orderEntity = otBll.GetModel(otid);
        Application["orderNumber"] = orderEntity.order_num;
        Application["is_delivery"] = orderEntity.is_delivery == 0 ? "未发货" : "已发货";
        Application["couNumber"] = orderEntity.courier_number;
    }
    #region 保存信息==============================
    protected void Button1_Click(object sender, EventArgs e)
    {
        int otid = Convert.ToInt32(Request["orderid"].ToString());
        Cms.BLL.C_order otBll = new Cms.BLL.C_order();
        Cms.Model.C_order orderEntity = otBll.GetModel(otid);
        orderEntity.is_receiving = 1;
        if (orderEntity.is_payment == 1)
        {
            if (otBll.Update(orderEntity))
            {
                JscriptMsg("收货成功！", "/shop/myOrder.aspx", "Success");
            }
            else
            {
                JscriptMsg("收货失败！", "NotJump", "Error");
            }
        }
        else
        {
            JscriptMsg("申请失败！该订单未支付", "NotJump", "Error");
        }
    }
    #endregion
    #region 提示框=================================
    public void JscriptMsg(string msgtitle, string url, string msgcss)
    {
        string msbox = "";
        if (url == "back")
        {
            msbox = "jsdialog(\"提示\", \"" + msgtitle + "\",\"" + url + "\", \"\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }
        else
        {
            msbox = "jsprintWeb(\"" + msgtitle + "\", \"" + url + "\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }

    }
    #endregion
}