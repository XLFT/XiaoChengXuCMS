﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="exchangePass.aspx.cs" Inherits="shop_exchangePass" %>

<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="/admin/scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <script type="text/javascript" src="/admin/js/layout.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
       <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0);" class="page-title">核销密码</a>
    </header>
    

 <section class="main main-product">
        <div class="p-contain">
        	<div class="p-vip">
            	<p>请输入本店的核销密码</p>
                <div class="vip-input">
                	<input name="" id="txtPass" runat="server" type="text" placeholder="核销密码" />
                </div>
               
                

                <div class="product-button1">
                   <div class="product-button1 " style=" text-align:center;">
                <asp:Button ID="Button1" runat="server" CssClass="login_ipt" Text="确认" 
                        onclick="Button1_Click"></asp:Button>
                </div>
            </div>
        </div>
     <uc2:footer ID="footer2" runat="server" />
     <uc3:bottom ID="bottom2" runat="server" />
    </section>
    </form>
</body>
</html>
