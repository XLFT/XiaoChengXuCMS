﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Cms.Common;
public partial class shop_integral : System.Web.UI.Page
{
    protected int user_id;//会员id
    protected string classIdList;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();
            this.user_id = userModel.id;
            Cms.BLL.C_user bll = new Cms.BLL.C_user();
            Cms.Model.C_user model = new Cms.BLL.C_user().GetModel(user_id);
            wxuser.userinfo xw = new wxuser.userinfo();
            try
            {
                xw = wxuser.getuserinfo(model.openid);
                model.userallscore = Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userallscore)).ToString());
                model.userscore = Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userscore)).ToString());
                model.userYesScore = Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userallscore)).ToString()) - Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userscore)).ToString());
                model.userlevel = xw.userlevel;
                model.shopname = xw.shopname;
                bll.Update(model);
            }
            catch (Exception ex)
            {
                JscriptMsg(ex.Message.ToString(), "NotJump", "Error");
            }
            Application["usercard"] = model.usercard;
            Application["userscore"] = model.userscore;
            Application["headimgurl"] = model.headimgurl;
            bind_RepIntegral();

            //产品列表
            if (Request["classid"] == null || Request["classid"].ToString() == "")
            {
                classIdList = ToAspx.getcloumnid(94);
            }
            else
            {
                classIdList = ToAspx.getcloumnid(Convert.ToInt32(Request["classid"].ToString()));
            }
            BindDate(classIdList);
        }
        else
        {
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();
            this.user_id = userModel.id;
            Cms.BLL.C_user bll = new Cms.BLL.C_user();
            Cms.Model.C_user model = new Cms.BLL.C_user().GetModel(user_id);
            wxuser.userinfo xw = new wxuser.userinfo();
            try
            {
                xw = wxuser.getuserinfo(model.openid);
                model.userallscore = Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userallscore)).ToString());
                model.userscore = Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userscore)).ToString());
                model.userYesScore = Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userallscore)).ToString()) - Convert.ToInt32(Math.Round(Convert.ToDecimal(xw.userscore)).ToString());
                model.userlevel = xw.userlevel;
                model.shopname = xw.shopname;
                bll.Update(model);
            }
            catch (Exception ex)
            {
                JscriptMsg(ex.Message.ToString(), "NotJump", "Error");
            }
            Application["usercard"] = model.usercard;
            Application["userscore"] = model.userscore;
            Application["headimgurl"] = model.headimgurl;
            bind_RepIntegral();

            //产品列表
            if (Request["classid"] == null || Request["classid"].ToString() == "")
            {
                classIdList = ToAspx.getcloumnid(94);
            }
            else
            {
                classIdList = ToAspx.getcloumnid(Convert.ToInt32(Request["classid"].ToString()));
            }
            BindDate(classIdList);
        }
    }
    #region 赋值积分产品=====================
    public void bind_RepIntegral()
    {
        DataTable dt = new Cms.BLL.C_Column().GetList("parentId=94").Tables[0];
        if (dt != null && dt.Rows.Count > 0)
        {
            RepIntegralColumn.DataSource = dt;
            RepIntegralColumn.DataBind();
        }

    }
    #endregion

    #region 赋值操作========================
    public void BindDate(string classIdList)
    {
        DataSet dsProduct = Cms.DBUtility.DbHelperSQL.Query("SELECT * FROM ( SELECT ROW_NUMBER() OVER (order by T.orderNumber desc )AS Row, T.*  from C_article T WHERE parentId in (" + classIdList + ")  ) TT WHERE TT.Row between 0 and 4");
        if (dsProduct != null && dsProduct.Tables[0].Rows.Count > 0)
        {
            RepIntegral.DataSource = dsProduct.Tables[0].DefaultView;
            RepIntegral.DataBind();
        }
        else
        {
            RepIntegral.DataSource = dsProduct.Tables[0].DefaultView;
            RepIntegral.DataBind();
        }

    }
    /// <summary>
    /// 获取商品积分
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getIntegral(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = dt.Rows[0]["userscore"].ToString();
            }
        }
        return result;
    }
    /// <summary>
    /// 获取商品市场价格
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getMprice(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = Convert.ToDecimal(dt.Rows[0]["marketPrice"]).ToString("0.00");
            }
        }
        return result;
    }
    #endregion

    #region 添加日志
    public void setlog(string str, string sname)
    {
        Cms.BLL.C_admin_log cm = new Cms.BLL.C_admin_log();
        Cms.Model.C_admin_log mc = new Cms.Model.C_admin_log();
        mc.remark = str;
        mc.action_type = "微信信息";
        mc.user_name = sname;
        mc.add_time = DateTime.Now;
        cm.Add(mc);
    }
    #endregion

    #region 签到领取积分=============================
    protected void LinkButton1_Click(object sender, EventArgs e)
    {


        Cms.BLL.C_user bll = new Cms.BLL.C_user();
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        Cms.Model.C_user model = new Cms.BLL.C_user().GetModel(userModel.id);

        wxuser.userinfo xw = new wxuser.userinfo();
        wxuser.userinfo userinfo = new wxuser.userinfo();
        DateTime todaybegin = DateTime.Parse(DateTime.Now.ToShortDateString());
        DateTime mingtianBegin = todaybegin.AddDays(1);
        
        //限制一天只能签到一次
        if (!Cms.DBUtility.DbHelperSQL.Exists("select count(1) from C_integral_rec where scorename='签到领取积分' and user_id=" + model.id + "and updateTime>='" + todaybegin + "' and updateTime<'" + mingtianBegin + "'"))
        {
            try
            {
                
                //
                xw = wxuser.getUserSign(model.openid, model.usercard);
                setlog(xw.content, xw.result);
                if (xw.result == "更新成功")
                {
                    userinfo = wxuser.getuserinfo(model.openid);
                    if (userinfo.result == "获取成功")
                    {
                        model.userallscore = Convert.ToInt32(Math.Round(Convert.ToDecimal(userinfo.userallscore)).ToString());
                        model.userscore = Convert.ToInt32(Math.Round(Convert.ToDecimal(userinfo.userscore)).ToString());
                        model.userYesScore = Convert.ToInt32(Math.Round(Convert.ToDecimal(userinfo.userallscore)).ToString()) - Convert.ToInt32(Math.Round(Convert.ToDecimal(userinfo.userscore)).ToString());
                        model.userlevel = xw.userlevel;
                        model.shopname = xw.shopname;
                        bll.Update(model);
                        //签到领取积分记录表
                        Cms.BLL.C_integral_rec integral_BLL = new Cms.BLL.C_integral_rec();
                        Cms.Model.C_integral_rec integral_model = new Cms.Model.C_integral_rec();
                        integral_model.article_id = 1;
                        integral_model.user_id = model.id;
                        integral_model.usercard = model.usercard;
                        integral_model.openid = model.openid;
                        integral_model.numberid = "2";
                        integral_model.scorename = "签到领取积分";
                        integral_model.title = "签到领取积分";
                        integral_model.wescore = 2000;//送多少积分
                        integral_model.quantity = 1;
                        integral_model.type = 0;
                        integral_model.updateTime = DateTime.Now;
                        int count = integral_BLL.Add(integral_model);
                        if (count > 0)
                        {
                            JscriptMsg("签到成功！", "/shop/integral.aspx", "Success");
                        }
                        else
                        {
                            JscriptMsg("签到失败！", "NotJump", "Error");
                        }
                    }
                    else
                    {
                        setlog(userinfo.content, userinfo.result);
                        JscriptMsg("签到失败！", "NotJump", "Error");
                    }
                }
                else
                {
                   
                    JscriptMsg("您今日已经签到过了！", "NotJump", "Error");
                }
            }
            catch (Exception ex)
            {

                JscriptMsg(ex.Message.ToString(), "NotJump", "Error");
            }
        }
        else
        {
            JscriptMsg("您今日已经签到过了！", "NotJump", "Error");
        }


    }
    #endregion

    #region 提示框=================================
    public void JscriptMsg(string msgtitle, string url, string msgcss)
    {
        string msbox = "";
        if (url == "back")
        {
            msbox = "jsdialog(\"提示\", \"" + msgtitle + "\",\"" + url + "\", \"\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }
        else
        {
            msbox = "jsprintWeb(\"" + msgtitle + "\", \"" + url + "\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }

    }
    #endregion
}