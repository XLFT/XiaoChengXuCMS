﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="looseSearch.aspx.cs" Inherits="shop_looseSearch" %>

<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/diamondresp.css" rel="stylesheet" />
    <!--<link href="css/normalize.min.css" rel="stylesheet">-->
    <link href="css/ion.rangeSlider.css" rel="stylesheet" />
    <link href="css/ion.rangeSlider.skinNice.css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/admin/scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <script type="text/javascript" src="/admin/scripts/datepicker/WdatePicker.js"></script>
    <script type="text/javascript">
   
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0)" class="page-title">裸钻搜索</a>
    </header>
    <section class="main main-product">
        <div class="p-contain">
        	<div class="search-title">
            	钻石形状
            </div>
                <div class="rule-multi-checkbox multi-checkbox">
                <asp:CheckBoxList ID="shape" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                </asp:CheckBoxList>
                </div>

            <div class="clear"></div>
            <div class="container-fluid page_cont">
                <div class="search-title">
                	价格:&nbsp;(搜索的价格范围)
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <div class="slider_box">
                      <input type="text" id="range_price" runat="server" />
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <button class="btn btn-primary btn-lg btn-block change_btn" id="Cprice">缩小价格选择范围</button>
                  </div>
                </div>
                <div class="search-title">
                	钻重:&nbsp;(搜索的钻重范围)
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <div class="slider_box">
                      <input type="text" id="range_carat" runat="server"/>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <button class="btn btn-primary btn-lg btn-block change_btn" id="Ccarat">扩大钻重选择范围</button>
                  </div>
                </div>
            </div>
            <div class="search-title">
            	颜色
            </div>
                <div class="rule-multi-checkboxcolor">
                <asp:CheckBoxList ID="color" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                </asp:CheckBoxList>
                </div>
            <div class="search-title">
            	净度
            </div>
             <div class="rule-multi-checkboxcolor">
                <asp:CheckBoxList ID="clarity" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                </asp:CheckBoxList>
                </div>
            <div class="search-title">
            	切工
            </div>
            <div class="rule-multi-checkboxcolor">
                <asp:CheckBoxList ID="cut" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                </asp:CheckBoxList>
                </div>
                 <div class="product-button">
                <input type="hidden" id="article_id" runat="server" value="" />
                <asp:Button ID="Button1" runat="server" CssClass="buy-button" Text="条件重置" 
                        onclick="Button1_Click"></asp:Button>
                <asp:Button ID="Button2" runat="server" CssClass="add-button" Text="搜索钻石" 
                        onclick="Button2_Click"></asp:Button>
                </div>
        </div>
             <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
</section>
    <script type="text/javascript" src="js/ion.rangeSlider.min.js"></script>
    <script src="js/looseSearch.js" type="text/javascript"></script>
    </form>
</body>
</html>
