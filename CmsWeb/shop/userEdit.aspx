﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="userEdit.aspx.cs" Inherits="shop_userEdit" %>
<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/admin/scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <script type="text/javascript" src="/admin/scripts/datepicker/WdatePicker.js"></script>
    <script type="text/javascript" src="/admin/js/layout.js"></script>

</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="/shop/user.aspx" class="page-title">个人信息</a>
    </header>
    <section class="main main-product">
        <div class="p-contain">
        	<div class="info-t">
            	<div class="info-con">
                    <dl>
                        <dt>您的电话：</dt>
                        <dd><%=Application["telphone"]%></dd>
                        <div class="clear"></div>
                    </dl>
                    <dl>
                        <dt>会员名：</dt>
                        <dd><%=Application["username"]%></dd>
                        <div class="clear"></div>
                    </dl>
                    <dl>
                        <dt>会员卡号：</dt>
                        <dd><%=Application["usercard"]%></dd>
                        <div class="clear"></div>
                    </dl>
                </div>
            </div>
            <div class="info-t">
            	<div class="info-con">
                    <dl>
                        <dt><span>*</span>您的姓名：</dt>
                        <dd><input name="" type="text" id="name" runat="server" class="info-input"/></dd>
                        <div class="clear"></div>
                    </dl>
                    <dl>
                        <dt><span>*</span>您的性别：</dt>
                        <dd>
                        <div class="rule-multi-radio">
                        <asp:RadioButtonList ID="sex" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <asp:ListItem Value="M">男</asp:ListItem>
                        <asp:ListItem Value="W">女</asp:ListItem>
                        </asp:RadioButtonList>
                        </div>
                        </dd>
                        <div class="clear"></div>
                    </dl>
                    <dl>
                      <dt><span>*</span>您的生日：</dt>
                        <dd>
                            <div class="input-date">
                            <asp:TextBox ID="birthday" runat="server" CssClass="input date" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"
                            datatype="/^\s*$|^\d{4}\-\d{1,2}\-\d{1,2}\s{1}(\d{1,2}:){2}\d{1,2}$/" errormsg="请选择正确的日期"
                            sucmsg=" " />
                            <i>日期</i>
                            </div>
                        </dd>
                        <div class="clear"></div>
                    </dl>
                    <dl>
                        <dt>结婚日期：</dt>
                        <dd>
                             <div class="input-date">
                            <asp:TextBox ID="marryday" runat="server" CssClass="input date" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"
                            datatype="/^\s*$|^\d{4}\-\d{1,2}\-\d{1,2}\s{1}(\d{1,2}:)$/" errormsg="请选择正确的日期"
                            sucmsg=" " />
                            <i>日期</i>
                            </div>
                        </dd>
                        <div class="clear"></div>
                    </dl>
                    <dl>
                        <dt>居住地址：</dt>
                        <dd>
                        <input name="" type="text" id="useraddress" runat="server" class="info-input"/>
                        </dd>
                        <div class="clear"></div>
                    </dl>
                    <dl>
                        <dt>所属门店：</dt>
                        <dd>
                        <asp:DropDownList ID="parentId" runat="server" Width="150px" style=" margin-top:6px;" datatype="*" sucmsg=" ">
                        <asp:ListItem Value="">微信门店</asp:ListItem>
                        </asp:DropDownList>
                           
                        </dd>
                        <div class="clear"></div>
                    </dl>
                </div>
            </div>
        	<div class="product-button1">
            <asp:Button ID="Button1" runat="server" CssClass="login_ipt" Text="保存信息" 
                    onclick="Button1_Click"></asp:Button>
            </div>
        </div>
     <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
</section>
    </form>
</body>
</html>
