﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="myOrder.aspx.cs" Inherits="shop_myOrder" %>

<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.1.11.1.js"></script>
    <script type="text/javascript">
        (function ($) {
            $.fn.extend({
                "jTab": function (o) {
                    o = $.extend({
                        menutag: "", 	//选项卡按钮标签
                        boxtag: "",     //选项卡内容标签
                        cur: 0, 	  	//选项卡的默认索引值
                        act: "mouseover", //事情触发的默认事件可以是"click"
                        fade: 0, 		//淡入的时间
                        auto: false, //false,true表示开关定时器
                        autoTime: 3000	//定时执行的时间
                    }, o)
                    $(o.menutag).eq(0).addClass("cur");
                    $(o.boxtag).eq(0).siblings().hide();
                    $(o.menutag).bind(o.act, function () {
                        index = $(o.menutag).index(this);
                        $(this).addClass("cur").siblings().removeClass("cur")
                        $(o.boxtag).eq(index).show(o.fade).siblings().hide();
                    })

                }
            })
        })(jQuery);
    </script>
    <script type="text/javascript">
        $(function () {
            $(".tab").jTab({
                menutag: ".tab-m>li",
                boxtag: ".tab-box>div",
                cur: 0,
                act: "mouseover",
                fade: 0,
                auto: false,
                autoTime: 3000
            })
        })
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
        <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0);" class="page-title">我的订单</a>
    </header>
    <section class="main main-product">
        <div class="p-contain">
        	<div class="tab">
                <ul class="tab-m">
                  <li><a href="javascript:void(0)"><span>待付款</span></a></li>
                  <li><a href="javascript:void(0)"><span>已支付</span></a></li>
                  <li><a href="javascript:void(0)"><span>已完成</span></a></li>
                  <div class="clear"></div>
                </ul>
                <div class="tab-box">
                    <div class="thfnxw">
                        <div class="mine-order">
                            <ul>
                            <asp:Repeater runat="server" ID="RepOrderList" OnItemDataBound="RepOrderList_ItemDataBound">
                            <ItemTemplate>
                                <li>
                                    <h2>下单时间：<%#Eval("updateTime")%>   <b style=" float:right;">¥：<%#Convert.ToDecimal(Eval("price_sum")).ToString("0.00")%></b>
                                    </h2>
                            <asp:Repeater runat="server" ID="RepOrderListSub">
                            <ItemTemplate>
                                    <div class="m-order-goods">
                                        <div class="m-order-goods1">
                                            <a href="/shop/orderDetail.aspx?id=<%#Eval("order_id")%>">
                                                <img src="<%#getPhoto(Eval("article_id").ToString())%>" class="m-oeder-img">
                                                <div class="m-order-name">
                                                    <h3><%#Eval("title")%></h3>
                                                    <p>订单编号：<%#Eval("order_num")%></p>
                                                    <p>价格：¥ <%#Convert.ToDecimal(Eval("price")).ToString("0.00")%><span>数量：<%#Eval("quantity")%></span></p>
                                                   
                                                </div>
                                                <div class="clear"></div>
                                            </a>
                                        </div>
                                    </div>
                            </ItemTemplate>
                            </asp:Repeater> 
                            <div class="status">
                             <span><%#Eval("is_payment").ToString() == "0" ? "<a href='/api/wxpay/JsApiPayPage.aspx?orderid=" + Eval("id") + "' class='fukuan'>付款</a>" : "<a href='javascript:void(0);'>已支付</a>"%></span>
                            <span>  <%#getIsRefund(Eval("is_refund").ToString(), Eval("id").ToString())%></span>
                            <span><%#getIsDelivery(Eval("is_delivery").ToString(), Eval("id").ToString())%></span>
                            <span><%#Eval("is_transaction").ToString() == "0" ? "<a href='javascript:void(0);'>未完成</a>" : "<a href='javascript:void(0);'>已完成</a>"%></span>
                                   </div>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                            <%#RepOrderList.Items.Count == 0 ? "<div style='text-align:center;'>暂无购物记录</div>" : ""%>
                            </FooterTemplate>
                            </asp:Repeater> 
                            </ul>
                        </div>
                    </div>
                    <div class="thfnxw">
                        <div class="mine-order">
                            <ul>
                           <asp:Repeater runat="server" ID="RepIsNo" OnItemDataBound="RepIsNo_ItemDataBound">
                            <ItemTemplate>
                                <li>
                                    <h2>下单时间：<%#Eval("updateTime")%>   <b style=" float:right;">¥：<%#Convert.ToDecimal(Eval("price_sum")).ToString("0.00")%></b></h2>
                                       <asp:Repeater runat="server" ID="RepIsNoSub">
                            <ItemTemplate>
                                    <div class="m-order-goods">
                                        <div class="m-order-goods1">
                                            <a href="/shop/orderDetail.aspx?id=<%#Eval("order_id")%>">
                                                <img src="<%#getPhoto(Eval("article_id").ToString())%>" class="m-oeder-img">
                                                <div class="m-order-name">
                                                    <h3><%#Eval("title")%></h3>
                                                    <p>订单编号：<%#Eval("order_num")%></p>
                                                    <p>价格：¥ <%#Convert.ToDecimal(Eval("price")).ToString("0.00")%><span>数量：<%#Eval("quantity")%></span></p>
                                                   
                                                </div>
                                                <div class="clear"></div>
                                            </a>
                                        </div>
                                    </div>
                            </ItemTemplate>
                            </asp:Repeater> 
                            <div class="status">
                            <%-- <a href='/api/payment/clientRefund.aspx?orderid=" + Eval("id") + "'>申请退款</a>--%>
                            <span><%#Eval("is_payment").ToString() == "0" ? "<a href=/api/wxpay/JsApiPayPage.aspx?orderid=" + Eval("id") + "' class='fukuan'>付款</a>" : "<a href='javascript:void(0);'>已支付</a>"%></span>
                            <span>  <%#getIsRefund(Eval("is_refund").ToString(), Eval("id").ToString())%></span>
                            <span><%#getIsDelivery(Eval("is_delivery").ToString(), Eval("id").ToString())%></span>
                            <span><%#Eval("is_transaction").ToString() == "0" ? "<a href='javascript:void(0);'>未完成</a>" : "<a href='javascript:void(0);'>已完成</a>"%></span>
                                    </div>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                            <%#RepIsNo.Items.Count == 0 ? "<div style='text-align:center;'>暂无购物记录</div>" : ""%>
                            </FooterTemplate>
                            </asp:Repeater> 
                            </ul>
                        </div>
                    </div>
                    <div class="thfnxw">
                        <div class="mine-order">
                            <ul>
                            <asp:Repeater runat="server" ID="RepIsYes" OnItemDataBound="RepIsYes_ItemDataBound">
                            <ItemTemplate>
                                <li>
                                    <h2>下单时间：<%#Eval("updateTime")%>   <b style=" float:right;">¥：<%#Convert.ToDecimal(Eval("price_sum")).ToString("0.00")%></b></h2>
                            <asp:Repeater runat="server" ID="RepIsYesSub">
                            <ItemTemplate>
                                    <div class="m-order-goods">
                                        <div class="m-order-goods1">
                                            <a href="/shop/orderDetail.aspx?id=<%#Eval("order_id")%>">
                                                 <img src="<%#getPhoto(Eval("article_id").ToString())%>" class="m-oeder-img">
                                                <div class="m-order-name">
                                                    <h3><%#Eval("title")%></h3>
                                                    <p>订单编号：<%#Eval("order_num")%></p>
                                                    <p>价格：¥ <%#Convert.ToDecimal(Eval("price")).ToString("0.00")%><span>数量：<%#Eval("quantity")%></span></p>
                                                   
                                                </div>
                                                <div class="clear"></div>
                                            </a>
                                        </div>
                                    </div>
                            </ItemTemplate>
                            </asp:Repeater> 
                           <div class="status">
                            <span><%#Eval("is_payment").ToString() == "0" ? "<a href='/api/wxpay/JsApiPayPage.aspx?orderid=" + Eval("id") + "' class='fukuan'>付款</a>" : "<a href='javascript:void(0);'>已支付</a>"%></span>
                            <span>  <%#getIsRefund(Eval("is_refund").ToString(), Eval("id").ToString())%></span>
                            <span><%#getIsDelivery(Eval("is_delivery").ToString(), Eval("id").ToString())%></span>
                            <span><%#Eval("is_transaction").ToString() == "0" ? "<a href='javascript:void(0);'>未完成</a>" : "<a href='javascript:void(0);'>已完成</a>"%></span>
                            </div>
                            </li>
                            </ItemTemplate>
                            <FooterTemplate>
                            <%#RepIsYes.Items.Count == 0 ? "<div style='text-align:center;'>暂无购物记录</div>" : ""%>
                            </FooterTemplate>
                            </asp:Repeater> 
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
    </section>
</section>
    </form>
</body>
</html>
