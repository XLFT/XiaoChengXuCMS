﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_productMore : System.Web.UI.Page
{
    protected int user_id;//会员id
    protected string classIdList;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind();
        }
    }
    public void Bind()
    {
        int pagenow = 0;
        string classid = "";
        string orderStr = "";
        if (Request.Params["page"] != null)
        {
            pagenow = Convert.ToInt32(Request.Params["page"]);
        }
        if (Request.Params["classid"] != null)
        {
            classid = Request.Params["classid"];
        }
        if (Request.Params["type_price"] == "1")
        {
            orderStr = "order by price asc";
        }
        DataSet ds = new Cms.BLL.C_article().GetList(classid);
        int count = ds.Tables[0].Rows.Count;
        //if (count % 4 == 1)
        //{
        int start = (pagenow - 1) * 4 + 1;
        int end = pagenow * 4;

        //string tearname = new Cms.BLL.C_Column().GetModel(pid).name.ToString();
        DataSet ds_teacher = Cms.DBUtility.DbHelperSQL.Query("SELECT * FROM ( SELECT ROW_NUMBER() OVER (order by T.orderNumber desc )AS Row, T.*  from View_product T WHERE " + classid + "  ) TT WHERE TT.Row between " + start + " and " + end + orderStr);
        //ds = bll.GetListByPage("parentId=" + pid, "updatetime desc", start, end);
        if (ds_teacher != null && ds_teacher.Tables[0].Rows.Count > 0)
        {
            RepList.DataSource = ds_teacher.Tables[0].DefaultView;
            RepList.DataBind();
        }
        else
        {
            RepList.DataSource = ds_teacher.Tables[0].DefaultView;
            RepList.DataBind();
        }
        //Response.Write(count%4);
        //}
    }
     protected void RepList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int id = Convert.ToInt32(((HiddenField)e.Item.FindControl("Fielddocid")).Value);
        switch (e.CommandName)
        {
            case "lbtnLess":
                update(id, "-");
                break;
        }
    }
    public void update(int id, string action)
    {

        Cms.BLL.C_collect bll = new Cms.BLL.C_collect();
        Cms.Model.C_collect model = new Cms.Model.C_collect();
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        this.user_id = userModel.id;

        int articleId = id;//文章ID
        DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + articleId).Tables[0];
        if (dt.Rows.Count > 0)
        {
            model.article_id = articleId;
            model.user_id = user_id;
            model.title = new Cms.BLL.C_article().GetModel(articleId).title.ToString();
            model.price = Convert.ToDecimal(dt.Rows[0]["price"]);
            model.integral = Convert.ToInt32(dt.Rows[0]["userscore"].ToString() == "" ? "0" : dt.Rows[0]["userscore"].ToString());
            model.updateTime = DateTime.Now;
            if (Cms.DBUtility.DbHelperSQL.Exists("select count(1) from C_collect where article_id=" + articleId + "and user_id=" + user_id))
            {
                JscriptMsg("您已收藏过了！", "NotJump", "Error");
            }
            else
            {
                int result = bll.Add(model);
                if (result > 0)
                {
                    JscriptMsg("收藏成功！", "NotJump", "Success");
                }
                else
                {
                    JscriptMsg("收藏失败！", "NotJump", "Error");
                }
            }

        }
    }

    public string getPrice(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = Convert.ToDecimal(dt.Rows[0]["price"]).ToString("0.00");
            }
        }
        return result;
    }

    public string getMprice(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = Convert.ToDecimal(dt.Rows[0]["marketPrice"]).ToString("0.00");
            }
        }
        return result;
    }

    #region 提示框=================================
    public void JscriptMsg(string msgtitle, string url, string msgcss)
    {
        string msbox = "";
        if (url == "back")
        {
            msbox = "jsdialog(\"提示\", \"" + msgtitle + "\",\"" + url + "\", \"\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }
        else
        {
            msbox = "jsprintWeb(\"" + msgtitle + "\", \"" + url + "\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }

    }
    #endregion
}