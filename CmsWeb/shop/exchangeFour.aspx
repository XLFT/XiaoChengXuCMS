﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="exchangeFour.aspx.cs" Inherits="shop_exchangeFour" %>
<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
       <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0);" class="page-title">积分兑换</a>
    </header>
    <section class="main main-product">
        <div class="p-contain">
            <div class="mine-order gift-list">
                <ul>
                    <asp:Repeater runat="server" ID="RepOrderList" OnItemDataBound="RepOrderList_ItemDataBound">
                <ItemTemplate>
                    <li>
                        <h2>兑换时间：<%#Eval("updateTime")%> </h2>
                         <asp:Repeater runat="server" ID="RepOrderListSub">
                            <ItemTemplate>
                        <div class="m-order-goods">
                            <a href="/shop/integralDetail.aspx?id=<%#Eval("article_id")%>">
                                <img src="<%#getPhoto(Eval("article_id").ToString())%>" class="m-oeder-img">
                                <div class="m-order-name">
                                    <h3><%#Eval("title")%></h3>
                                    <p><span><b>积分兑换：</b><%#Eval("integral")%>积分<del> 价格：¥ <%#Convert.ToDecimal(Eval("price")).ToString("0.00")%> </del><i>数量：<%#Eval("quantity")%></i></span></p>
                                </div>
                                <div class="clear"></div>
                            </a>
                          <%--  <span>卡号：<%=Application["usercard"]%></span>--%>
                        </div>
                            </ItemTemplate>
                            </asp:Repeater> 
                       
                    </li>
                </ItemTemplate>
                </asp:Repeater>
                </ul>
            </div>
            <div class="inte-ewm dbe-borbg">
            	<h2>提货二维码<span>请在提货时出示此二维码</span></h2>
                <div class="integ-ewm">
                	<img src="images/ewm.jpg" id="ewm" runat="server" alt=""/>
                </div>
            </div>
            <div class="gift-addr dbe-borbg">
            	<h3>订单号：<%=Application["order_num"] %><span>待自提</span></h3>
                <div class="pd-l10">
                    <p>自提地址：</p>
                    <h2><asp:Label ID="storename" runat="server" Text="Label"></asp:Label></h2>
                <p>门店地址：<asp:Label ID="address" runat="server" Text="Label"></asp:Label></p>
                <p>门店电话：<asp:Label ID="tel" runat="server" Text="Label"></asp:Label></p>
                </div>
            </div>
            <%--<div class="product-button1">
                <a href="#" class="login_ipt">查看预约情况</a>
            </div>--%>
        </div>
    </section>
     <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
</section>
    </form>
</body>
</html>
