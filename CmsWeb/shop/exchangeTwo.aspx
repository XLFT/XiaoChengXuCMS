﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="exchangeTwo.aspx.cs" Inherits="shop_exchangeTwo" %>
<%@ Register Src="~/shop/Control/nav.ascx" TagName="nav" TagPrefix="uc1" %>
<%@ Register Src="~/shop/Control/footer.ascx" TagName="footer" TagPrefix="uc2" %>
<%@ Register Src="~/shop/Control/bottom.ascx" TagName="bottom" TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="format-detection" content="telephone=no" />
    <title>DBE珠宝</title>
    <meta name="keywords" content="DBE珠宝" />
    <meta name="description" content="DBE珠宝" />
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="/admin/scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <script type="text/javascript" src="/admin/js/layout.js"></script>
    <script src="js/custom.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <section class="all">
    <header>
       <a href="javascript:history.back(-1);" class="back"></a>
        <a href="/shop/main.aspx" class="dbe-admin"></a>
        <a href="javascript:void(0);" class="page-title">积分兑换</a>
    </header>
    <section class="main main-product">
        <div class="p-contain">
            <div class="mine-order gift-list">
                <ul>
                   <asp:Repeater runat="server" ID="RepExchange" 
                        onitemcommand="RepExchange_ItemCommand">
                <ItemTemplate>
                    <li>
                        <h2>礼品清单<span>卡号：<%=Application["usercard"]%></span></h2>
                        <div class="m-order-goods">
                            <a href="/shop/integralDetail.aspx?id=<%#Eval("article_id") %>">
                                <img src="<%#getPhoto(Eval("article_id").ToString())%>" class="m-oeder-img">
                                <div class="m-order-name">
                                    <h3><%#Eval("title") %></h3>
                                    <p><span><b>积分兑换：</b><%#getIntegral(Convert.ToInt32(Eval("article_id")))%>积分<del> 价格：¥ <%#getMprice(Convert.ToInt32(Eval("article_id")))%> </del><i>数量：<%#Eval("quantity")%></i></span></p>
                                </div>
                                <div class="clear"></div>
                            </a>
                        &nbsp;&nbsp;&nbsp;</div>
                    </li>
                </ItemTemplate>
                </asp:Repeater>
            <asp:HiddenField ID="strDintegral" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="strQTotal" runat="server"></asp:HiddenField>
                </ul>
            </div>
            <div class="gift-addr dbe-borbg pd-l10">
            	<h2>门店自提
                <div class="dzselect"><span>请选择门店所在省份：</span>
                <asp:DropDownList ID="lbProvince" runat="server" AutoPostBack="True"
                        OnSelectedIndexChanged="lbProvince_SelectedIndexChanged">
                 </asp:DropDownList>
                <%--<span id="proString" runat="server">湖南省</span>--%></div>
                <div class="addbg">
                <div class="addres-list">
                	<p>请选择省份</p>
                	<ul>
                    <asp:Repeater runat="server" ID="Province" onitemcommand="Province_ItemDataBound">
                    <ItemTemplate >
                    
                    	<li  val="<%#Eval("ProvinceName") %>">
                        <asp:LinkButton  ID="lbtnIsMsg" CommandName="lbtnIsMsg" runat="server" >
                        <asp:HiddenField ID="Fielddocid" Value='<%#Eval("ProvinceID")%>' runat="server" />
                        <%#Eval("ProvinceName")%>
                          </asp:LinkButton>
                        </li>
                      
                    </ItemTemplate>
                    </asp:Repeater>
                    </ul>
                </div>
                </div>
                <div class="addrlist">
                	<ul>
                    <asp:Repeater runat="server" ID="Repeater1" onitemcommand="Repeater1_ItemDataBound">
                    <ItemTemplate >
                    	<li>
                        <a val="<%#Eval("id")%>">
                         
                        	<h3><%#Eval("storename") %></h3>
                            <p><span>门店地址：</span><%#Eval("address")%></p>
                            <p><span>门店电话：</span><%#Eval("tel") %></p>
                            </a>
                        </li>
                    </ItemTemplate>
                     </asp:Repeater>
                    </ul>
                </div>
            </div>
            <div class="gift-addr dbe-borbg pd-l10" style=" display:none;">

            	<h2><asp:Label ID="storename" runat="server" Text="Label"></asp:Label></h2>
                <p>门店地址：<asp:Label ID="address" runat="server" Text="Label"></asp:Label></p>
                <p>门店电话：<asp:Label ID="tel" runat="server" Text="Label"></asp:Label></p>
                <asp:HiddenField ID="HiddenField1" runat="server"></asp:HiddenField>
            </div>
            <div class="product-button1 product-button2">
            <asp:LinkButton ID="LinkButton1" CssClass="login_ipt" runat="server" 
                    onclick="LinkButton1_Click">提交兑换</asp:LinkButton>
<asp:LinkButton ID="LinkButton2" CssClass="login_ipt-xg" runat="server" 
                    onclick="LinkButton2_Click">取&nbsp;&nbsp;&nbsp;消</asp:LinkButton>
            </div>
        </div>
    </section>
     <uc2:footer ID="footer1" runat="server" />
     <uc3:bottom ID="bottom1" runat="server" />
</section>
    </form>
</body>
</html>
