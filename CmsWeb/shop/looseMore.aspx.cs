﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class shop_looseMore : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind();
        }
    }
    public void Bind()
    {
        int pagenow = 0;
        string classid = "";
        if (Request.Params["page"] != null)
        {
            pagenow = Convert.ToInt32(Request.Params["page"]);
        }
        if (Request.Params["classid"] != null)
        {
            classid = Request.Params["classid"];
        }
        DataSet ds = Cms.DBUtility.DbHelperSQL.Query("SELECT * FROM ( SELECT ROW_NUMBER() OVER (order by T.orderNumber desc )AS Row, T.*  from View_product T WHERE parentId =103 " + classid + "  ) TT ");
        int count = ds.Tables[0].Rows.Count;
        //if (count % 4 == 1)
        //{
        int start = (pagenow - 1) * 4 + 1;
        int end = pagenow * 4;

        //string tearname = new Cms.BLL.C_Column().GetModel(pid).name.ToString();
        DataSet ds_teacher = Cms.DBUtility.DbHelperSQL.Query("SELECT * FROM ( SELECT ROW_NUMBER() OVER (order by T.orderNumber desc )AS Row, T.*  from View_product T WHERE parentId =103 " + classid + "  ) TT  WHERE TT.Row between " + start + " and " + end);
        //ds = bll.GetListByPage("parentId=" + pid, "updatetime desc", start, end);
        if (ds_teacher != null && ds_teacher.Tables[0].Rows.Count > 0)
        {
            RepList.DataSource = ds_teacher.Tables[0].DefaultView;
            RepList.DataBind();
        }
        else
        {
            RepList.DataSource = ds_teacher.Tables[0].DefaultView;
            RepList.DataBind();
        }
        //Response.Write(count%4);
        //}
    }
    /// <summary>
    /// 价格
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getPrice(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = Convert.ToDecimal(dt.Rows[0]["price"]).ToString("0.00");
            }
        }
        return result;
    }
    /// <summary>
    /// 钻石重
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getLoose(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = dt.Rows[0]["main_stone_weight"].ToString();
            }
        }
        return result;
    }
    /// <summary>
    /// 颜色
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getColor(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = new Cms.BLL.C_article_category().GetModel(Convert.ToInt32(dt.Rows[0]["color"].ToString())).title;
            }
        }
        return result;
    }
    /// <summary>
    /// 净度
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getClarity(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = new Cms.BLL.C_article_category().GetModel(Convert.ToInt32(dt.Rows[0]["clarity"].ToString())).title;
            }
        }
        return result;
    }
    /// <summary>
    /// 切工
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getCut(int article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            DataTable dt = new Cms.BLL.C_article_product().GetList("article_id=" + article_id).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                result = new Cms.BLL.C_article_category().GetModel(Convert.ToInt32(dt.Rows[0]["cut"].ToString())).title;
            }
        }
        return result;
    }
}