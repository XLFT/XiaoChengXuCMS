﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="productMore.aspx.cs" Inherits="shop_productMore" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Repeater runat="server" ID="RepList" OnItemCommand="RepList_ItemCommand">
        <ItemTemplate>
            <li>
                <div class="p-product-con">
                    <a href="/shop/productDetail.aspx?id=<%#Eval("articleId") %>">
                        <img src="<%#Eval("photoUrl") %>" alt=""></a>
                    <asp:HiddenField ID="Fielddocid" Value='<%#Eval("articleId")%>' runat="server" />
                    <div class="p-product-name">
                        <h3>
                            <a href="/shop/productDetail.aspx?id=<%#Eval("articleId") %>">
                                <%#Eval("title").ToString().Substring(0, 8)%>...</a></h3>
                        <p>
                            <span>¥
                                <%#getPrice(Convert.ToInt32(Eval("articleId"))) %></span><br />
                            <del>原价：¥
                                <%#getMprice(Convert.ToInt32(Eval("articleId"))) %></del>
                            <asp:LinkButton ID="lbtnIsLess" CommandName="lbtnLess" runat="server" CssClass="p-collect"></asp:LinkButton>
                        </p>
                    </div>
                </div>
            </li>
        </ItemTemplate>
    </asp:Repeater>
    <div class="clear">
    </div>
    </form>
</body>
</html>
