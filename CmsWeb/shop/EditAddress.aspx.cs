﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Cms.Common;

public partial class shop_EditAddress : System.Web.UI.Page
{
    protected int user_id;//会员id
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();
            this.user_id = userModel.id;
            bind_Province(Province);
            bind_City(1, City);
            bind_District(1, District);
            if (Request["id"] != null)
            {
                int id = Convert.ToInt32(Request["id"].ToString());
                Bind_date(id);
            }
            else
            {

            }

        }
    }

    #region 省市===========================================
    /// <summary>
    /// 绑定省
    /// </summary>
    public void bind_Province(DropDownList lbCity)
    {
        DataSet ds = new Cms.BLL.C_Province().GetList("");
        lbCity.DataSource = ds.Tables[0].DefaultView;
        lbCity.DataValueField = "ProvinceID";
        lbCity.DataTextField = "ProvinceName";
        lbCity.DataBind();
    }
    protected void Province_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.Province.SelectedValue == "")
        {
            return;
        }
        int txtProvince = Utils.StrToInt(Province.SelectedValue.ToString(), 0);
        bind_City(txtProvince, City);

    }
    /// <summary>
    /// 绑定市
    /// </summary>
    public void bind_City(int Province, DropDownList lbCity)
    {
        DataSet ds = new Cms.BLL.C_City().GetList("ProvinceID=" + Province);
        if (ds != null && ds.Tables[0].Rows.Count > 0)
        {
            lbCity.DataSource = ds.Tables[0].DefaultView;
            lbCity.DataValueField = "CityID";
            lbCity.DataTextField = "CityName";
            lbCity.DataBind();
            int txtCityID = Utils.StrToInt(ds.Tables[0].Rows[0]["CityID"].ToString(), 0);
            bind_District(txtCityID, District);
        }
    }
    protected void City_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.City.SelectedValue == "")
        {
            return;
        }
        int txtCityID = Utils.StrToInt(City.SelectedValue.ToString(), 0);
        bind_District(txtCityID, District);
    }
    /// <summary>
    /// 绑定区
    /// </summary>
    public void bind_District(int CityID, DropDownList lbCity)
    {
        DataSet ds = new Cms.BLL.C_District().GetList("CityID=" + CityID);
        lbCity.DataSource = ds.Tables[0].DefaultView;
        lbCity.DataValueField = "DistrictID";
        lbCity.DataTextField = "DistrictName";
        lbCity.DataBind();
    }

    #endregion

    #region 赋值操作========================
    public void Bind_date(int id)
    {
        Cms.Model.c_user_address model = new Cms.BLL.c_user_address().GetModel(id);
        this.consignee.Value = model.consignee;
        this.cellphone.Value = model.cellphone;
        this.code.Value = model.code;
        this.Province.SelectedValue = model.location;
        bind_City(Convert.ToInt32(model.location), City);
        this.City.SelectedValue = model.city;
        bind_District(Convert.ToInt32(model.city), District);
        this.District.SelectedValue = model.county;
        this.street.Value = model.street;
        this.address.Value = model.address;
        this.is_default.SelectedValue = model.is_default.ToString();

    }
    #endregion

    #region 保存===============================
    protected void Button1_Click(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(this.Request.QueryString["id"] ?? "0");//id
        string action = this.Request.QueryString["action"] ?? "";//编辑：edit 添加：add
        switch (action)
        {
            case "add":
                this.DataAdd();
                break;
            case "edit":
                this.DataUpdate(id);
                break;
        }
    }
    public void DataAdd()
    {
        Cms.BLL.c_user_address bll = new Cms.BLL.c_user_address();
        Cms.Model.c_user_address model = new Cms.Model.c_user_address();
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        model.user_id = userModel.id;
        model.consignee = this.consignee.Value;
        model.cellphone = this.cellphone.Value;
        model.code = this.code.Value;
        model.location = Province.SelectedValue;
        model.city = City.SelectedValue;
        model.county = District.SelectedValue;
        model.street = this.street.Value;
        model.address = this.address.Value;
        model.is_default = Convert.ToInt32(this.is_default.SelectedValue);
        if (model.is_default == 1)
        {
            Cms.DBUtility.DbHelperSQL.ExecuteSql("update c_user_address set is_default=0 where user_id=" + model.user_id);
        }
        int result = bll.Add(model);
        if (result > 0)
        {
            if (Request["ReturnPage"] == null)
            {
                JscriptMsg("保存成功！", "/shop/myAddress.aspx", "Success");
            }
            else
            {
                JscriptMsg("保存成功！", Request["ReturnPage"].ToString(), "Success");
            }
        }
        else
        {
            JscriptMsg("保存失败！", "NotJump", "Error");
        }
    }

    public void DataUpdate(int id)
    {
        Cms.BLL.c_user_address bll = new Cms.BLL.c_user_address();
        Cms.Model.c_user_address model = new Cms.Model.c_user_address();
        Cms.Model.C_user userModel = adminUser.GetuserLoginState();
        model.user_id = userModel.id;
        model.id = id;
        model.consignee = this.consignee.Value;
        model.cellphone = this.cellphone.Value;
        model.code = this.code.Value;
        model.location = Province.SelectedValue;
        model.city = City.SelectedValue;
        model.county = District.SelectedValue;
        model.street = this.street.Value;
        model.address = this.address.Value;
        model.is_default = Convert.ToInt32(this.is_default.SelectedValue);
        if (model.is_default == 1)
        {
            Cms.DBUtility.DbHelperSQL.ExecuteSql("update c_user_address set is_default=0 where user_id=" + model.user_id);
        }
        if (bll.Update(model))
        {
            if (Request["ReturnPage"] == null)
            {
                JscriptMsg("保存成功！", "/shop/myAddress.aspx", "Success");
            }
            else
            {
                JscriptMsg("保存成功！", Request["ReturnPage"].ToString(), "Success");
            }
           
        }
        else
        {
            JscriptMsg("保存失败！", "NotJump", "Error");
        }
    }
    #endregion

    #region 提示框=================================
    public void JscriptMsg(string msgtitle, string url, string msgcss)
    {
        string msbox = "";
        if (url == "back")
        {
            msbox = "jsdialog(\"提示\", \"" + msgtitle + "\",\"" + url + "\", \"\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }
        else
        {
            msbox = "jsprintWeb(\"" + msgtitle + "\", \"" + url + "\", \"" + msgcss + "\")";
            ClientScript.RegisterClientScriptBlock(Page.GetType(), "JsPrint", msbox, true);
        }

    }
    #endregion
}