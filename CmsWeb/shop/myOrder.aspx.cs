﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class shop_myOrder : System.Web.UI.Page
{
    protected int user_id;//会员id
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cms.Model.C_user userModel = adminUser.GetuserLoginState();
            this.user_id = userModel.id;
            Bind_date(user_id);//赋值操作
        }
    }
    #region 赋值操作========================
    public void Bind_date(int user_id)
    {
        DataTable dt = new Cms.BLL.C_order().GetList("is_payment=0 and user_id=" + user_id + " order by id desc").Tables[0];
        if (dt != null && dt.Rows.Count > 0)
        {
            RepOrderList.DataSource = dt;
            RepOrderList.DataBind();
        }
        else
        {
            RepOrderList.DataSource = dt;
            RepOrderList.DataBind();
        }

        DataTable dtIsNO = new Cms.BLL.C_order().GetList("is_payment=1 and is_transaction=0 and user_id=" + user_id + " order by id desc").Tables[0];
        if (dtIsNO != null && dtIsNO.Rows.Count > 0)
        {
            RepIsNo.DataSource = dtIsNO;
            RepIsNo.DataBind();
        }
        else
        {
            RepIsNo.DataSource = dtIsNO;
            RepIsNo.DataBind();
        }
        DataTable dtIsYes = new Cms.BLL.C_order().GetList("is_transaction=1 and user_id=" + user_id + " order by id desc").Tables[0];
        if (dtIsYes != null && dtIsYes.Rows.Count > 0)
        {
            RepIsYes.DataSource = dtIsYes;
            RepIsYes.DataBind();
        }
        else
        {
            RepIsYes.DataSource = dtIsYes;
            RepIsYes.DataBind();
        }
    }

    public void RepOrderList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rep = e.Item.FindControl("RepOrderListSub") as Repeater;//找到里层的repeater对象
            DataRowView rowv = (DataRowView)e.Item.DataItem;//找到分类Repeater关联的数据项 
            int id = Convert.ToInt32(rowv["id"]); //获取填充子类的id 

            DataSet dsNavOne = new Cms.BLL.C_ordersub().GetList("order_id=" + id + " order by id desc");
            if (dsNavOne != null && dsNavOne.Tables[0].Rows.Count > 0)
            {
                rep.DataSource = dsNavOne.Tables[0].DefaultView;
                rep.DataBind();
            }
        }
    }
    public void RepIsNo_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rep = e.Item.FindControl("RepIsNoSub") as Repeater;//找到里层的repeater对象
            DataRowView rowv = (DataRowView)e.Item.DataItem;//找到分类Repeater关联的数据项 
            int id = Convert.ToInt32(rowv["id"]); //获取填充子类的id 

            DataSet dsNavOne = new Cms.BLL.C_ordersub().GetList("order_id=" + id + " order by id desc");
            if (dsNavOne != null && dsNavOne.Tables[0].Rows.Count > 0)
            {
                rep.DataSource = dsNavOne.Tables[0].DefaultView;
                rep.DataBind();
            }
        }
    }
    public void RepIsYes_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rep = e.Item.FindControl("RepIsYesSub") as Repeater;//找到里层的repeater对象
            DataRowView rowv = (DataRowView)e.Item.DataItem;//找到分类Repeater关联的数据项 
            int id = Convert.ToInt32(rowv["id"]); //获取填充子类的id 

            DataSet dsNavOne = new Cms.BLL.C_ordersub().GetList("order_id=" + id + " order by id desc");
            if (dsNavOne != null && dsNavOne.Tables[0].Rows.Count > 0)
            {
                rep.DataSource = dsNavOne.Tables[0].DefaultView;
                rep.DataBind();
            }
        }
    }
    /// <summary>
    /// 获取产品封面图
    /// </summary>
    /// <param name="article_id"></param>
    /// <returns></returns>
    public string getPhoto(string article_id)
    {
        string result = "";
        if (new Cms.BLL.C_article().Exists(Convert.ToInt32(article_id)))
        {
            result = new Cms.BLL.C_article().GetModel(Convert.ToInt32(article_id)).photoUrl.ToString();
        }
        return result;
    }

    public string getIsRefund(string IsRefund,string orderId)
    {
        string result = "";
        Cms.Model.C_order model = new Cms.BLL.C_order().GetModel(Convert.ToInt32(orderId));
        if (model.is_payment == 1)
        {
            if (IsRefund == "0")
            {
                result = "<a href='/shop/Refund.aspx?orderid=" + orderId + "'>申请退款</a>";
            }
            else if (IsRefund == "1")
            {
                result = "<a href='javascript:void(0);'>审核退款中</a>";
            }
            else if (IsRefund == "2")
            {
                result = "<a href='/api/wxpay/RefundPage.aspx?orderid=" + orderId + "'>确认退款</a>";
            }
            else if (IsRefund == "3")
            {
                result = "<a href='javascript:void(0);'>退款成功</a>";
            }
        }
        

        return result;
    }
    public string getIsDelivery(string IsDelivery, string orderId)
    {
        string result = "";
        if (IsDelivery == "0")
        {
            result = "<a href='javascript:void(0);'>未发货</a>";
        }
        else if (IsDelivery == "1")
        {
            int is_receiving = Convert.ToInt32(new Cms.BLL.C_order().GetModel(Convert.ToInt32(orderId)).is_receiving);
            if (is_receiving == 1)
            {
                result = "<a href='javascript:void(0);'>已收货</a>";
            }
            else
            {
                result = "<a href='/shop/Delivery.aspx?orderid=" + orderId + "'>确认收货</a>";
            }
        }
        

        return result;
    }
    #endregion
}